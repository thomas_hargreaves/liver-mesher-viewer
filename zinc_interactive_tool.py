#!/usr/bin/python
"""
PyZinc examples

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

Created on Mar 27, 2013

@author: hsorby
"""

try:
    from PySide import QtGui, QtCore, QtGui
except ImportError:
    from PyQt4 import QtCore, QtGui

from opencmiss.zinc.sceneviewerinput import Sceneviewerinput
from opencmiss.zinc.scenecoordinatesystem import \
        SCENECOORDINATESYSTEM_LOCAL, \
        SCENECOORDINATESYSTEM_WINDOW_PIXEL_TOP_LEFT
from opencmiss.zinc.element import Element, Elementbasis
from opencmiss.zinc.field import Field
from opencmiss.zinc.glyph import Glyph
from opencmiss.zinc.graphics import Graphics
from opencmiss.zinc.status import OK

import math
from zinc_helper import *
from zinc_undo import Undo
import zinc_interactive_static as zinc_static

# mapping from qt to zinc start
# Create a button map of Qt mouse buttons to Zinc input buttons
button_map = {QtCore.Qt.LeftButton: Sceneviewerinput.BUTTON_TYPE_LEFT,
              QtCore.Qt.MidButton: Sceneviewerinput.BUTTON_TYPE_MIDDLE,
              QtCore.Qt.RightButton: Sceneviewerinput.BUTTON_TYPE_RIGHT}


# Create a modifier map of Qt modifier keys to Zinc modifier keys
def modifier_map(qt_modifiers):
    """
    Return a Zinc SceneViewerInput modifiers object that is created from
    the Qt modifier flags passed in.
    """
    modifiers = Sceneviewerinput.MODIFIER_FLAG_NONE
    if qt_modifiers & QtCore.Qt.SHIFT:
        modifiers = modifiers | Sceneviewerinput.MODIFIER_FLAG_SHIFT

    return modifiers
# mapping from qt to zinc end

SELECTION_RUBBERBAND_NAME = 'selection_rubberband'


class NodeEditInfo(object):
    
    def __init__(self):
        self.reset()
    
    def reset(self):
        self._node = None
        self._graphics = None
        self._coordinateField = None
        self._orientationField = None
        self._glyphCentre = [0.0, 0.0, 0.0]
        self._glyphSize = [0.0, 0.0, 0.0]
        self._glyphScaleFactors = [0.0, 0.0, 0.0]
        self._variableScaleField = Field()        
        self._nearestElement = None
        self._elementCoordinateField = None
        self._createCoordinatesField = None
        
        
# selectionMode start
class SelectionMode(object):

    NONE = -1
    EXCLUSIVE = 0
    ADDITIVE = 1
    EDIT_POSITION = 2
    EDIT_VECTOR = 3
    CREATE = 4
    DELETE = 5
# selectionMode end


class ZincInteractiveTool(QtCore.QObject):

    QtCore.pyqtSignal = QtCore.Signal
    QtCore.pyqtSlot = QtCore.Slot

    update_volume = QtCore.Signal('dict')

    def __init__(self, zincwidget):
        # signal to update the volume
        super(ZincInteractiveTool, self).__init__(zincwidget)

        # Selection attributes
        self._nodeSelectMode = False
        self._nodeEditMode = False
        self._nodeEditVectorMode = False
        self._nodeCreateMode = False
        self._nodeConstrainMode = False
        self._nodeDeleteMode = False
        self._dataSelectMode = False
        self._1delemSelectMode = False
        self._2delemSelectMode = False
        self._elemSelectMode = False
        self._elemCreateMode = False
        self._selection_mode = SelectionMode.NONE
        self._selectionGroup = None
        self._selection_box = None
        self._ignore_mouse_events = False
        self._editModifier = QtCore.Qt.CTRL
        self._selectionModifier = QtCore.Qt.SHIFT
        self._additiveSelectionModifier = QtCore.Qt.ALT
        self._sceneviewer = None
        self._scenepicker = None
        self._zincwidget = zincwidget
        self._nodeEditInfo = NodeEditInfo()
        self._sceneSurfacesFilter = None
        self._createCoordinatesField = None

        self._selectionBox_setBaseSize = None
        self._selectionBox_setGlyphOffset = None

        self._selectionAlwaysAdditive = False
        self._selection_position_start = None
        self._handle_mouse_events = False

        self._nodeset_group = None
        self._element_group = None
        self._undo = Undo(self)
        self._nodeMove = None
        self._nodesMoved = []
        self._selected_nodes = []


    def setAdditiveSelectionModifier(self, modifierIn):
        self._additiveSelectionModifier = modifierIn

    def setEditModifier(self, modifierIn):
        self._editModifier = modifierIn

    def setSelectionModifier(self, modifierIn):
        self._selectionModifier = modifierIn

    def setSceneviewer(self, sceneviewer):
        self._sceneviewer = sceneviewer
        if self._sceneviewer.isValid():
            scene = self._sceneviewer.getScene()
            graphics_filter = self._sceneviewer.getScenefilter()
            self._scenepicker = scene.createScenepicker()
            region = scene.getRegion()

            fieldmodule = region.getFieldmodule()

            self._selectionGroup = fieldmodule.createFieldGroup()
            scene.setSelectionField(self._selectionGroup)

            self._scenepicker = scene.createScenepicker()
            self._scenepicker.setScenefilter(graphics_filter)
            sceneFilterModule = self._zincwidget.getContext().getScenefiltermodule()
            self._sceneSurfacesFilter = sceneFilterModule.createScenefilterOperatorAnd()
            surfacesFilter = sceneFilterModule.createScenefilterGraphicsType(Graphics.TYPE_SURFACES)
            self._sceneSurfacesFilter.appendOperand(graphics_filter)
            self._sceneSurfacesFilter.appendOperand(surfacesFilter)
            # If the standard glyphs haven't been defined then the
            # selection box will not be visible
            self.createSelectionBox(scene)

    def setScenefilter(self, scenefilter):
        self._scenepicker.setScenefilter(scenefilter)

    def setNodeSelection(self, enabled):
        self._nodeSelectMode = enabled

    def setNodeDeleteMode(self, enabled):
        self._nodeDeleteMode = enabled

    def setNodeEdit(self, enabled):
        self._nodeEditMode = enabled

    def setNodeConstrainToSurfacesMode(self, enabled):
        self._nodeConstrainMode = enabled

    def setNodeCreateMode(self, enabled):
        self._nodeCreateMode = enabled

    def setElemCreateMode(self, enabled):
        self._elemCreateMode = enabled

    def setNodeCreateCoordinatesField(self, coordinatesField):
        self._createCoordinatesField = coordinatesField

    def setSelectionModeAdditive(self, enabled):
        self._selectionAlwaysAdditive = enabled

    def setLineSelection(self, enabled):
        self._1delemSelectMode = enabled

    def setSurfacesSelection(self, enabled):
        self._2delemSelectMode = enabled

    def setSelectModeAll(self):
        self._nodeSelectMode = True
        self._dataSelectMode = True
        self._elemSelectMode = True

    def createSelectionBox(self, scene):
        if self._selection_box:
            previousScene = self._selection_box.getScene()
            previousScene.removeGraphics(self._selection_box)

        self._selection_box = scene.createGraphicsPoints()
        self._selection_box.setName(SELECTION_RUBBERBAND_NAME)
        self._selection_box.setScenecoordinatesystem(SCENECOORDINATESYSTEM_WINDOW_PIXEL_TOP_LEFT)

        attributes = self._selection_box.getGraphicspointattributes()
        attributes.setGlyphShapeType(Glyph.SHAPE_TYPE_CUBE_WIREFRAME)
        attributes.setBaseSize([10, 10, 0.9999])
        attributes.setGlyphOffset([1, -1, 0])
        self._selectionBox_setBaseSize = attributes.setBaseSize
        self._selectionBox_setGlyphOffset = attributes.setGlyphOffset
        self._selection_box.setVisibilityFlag(False)

    def getScenepicker(self):
        return self._scenepicker

    def getSelectionGroup(self):
        return self._selectionGroup

    def setPickingRectangle(self, coordinate_system, left, bottom, right, top):
        self._scenepicker.setSceneviewerRectangle(self._sceneviewer, coordinate_system, left, bottom, right, top)

    def setSelectionfilter(self, scenefilter):
        self._scenepicker.setScenefilter(scenefilter)

    def getSelectionfilter(self):
        result, scenefilter = self._scenepicker.getScenefilter()
        if result == OK:
            return scenefilter
        return None

    def _getNearestGraphic(self, x, y, domain_type):
        self._scenepicker.setSceneviewerRectangle(
            self._sceneviewer, SCENECOORDINATESYSTEM_LOCAL, x - 0.5, y - 0.5, x + 0.5, y + 0.5)
        nearest_graphics = self._scenepicker.getNearestGraphics()
        if nearest_graphics.isValid() and nearest_graphics.getFieldDomainType() == domain_type:
            return nearest_graphics

        return None

    def getNeareshGraphics(self):
        return self._scenepicker.getNearestGraphics()

    def getNearestGraphicsNode(self, x, y):
        return self._getNearestGraphic(x, y, Field.DOMAIN_TYPE_NODES)

    def getNearestGraphicsPoint(self, x, y):
        """
        Assuming given x and y is in the sending widgets coordinates 
        which is a parent of this widget.  For example the values given 
        directly from the event in the parent widget.
        """
        return self._getNearestGraphic(x, y, Field.DOMAIN_TYPE_POINT)

    def getNearestNode(self, x, y):
        self._scenepicker.setSceneviewerRectangle(self._sceneviewer, SCENECOORDINATESYSTEM_LOCAL, x - 0.5, y - 0.5, x + 0.5, y + 0.5)
        node = self._scenepicker.getNearestNode()

        return node

    def addPickedNodesToFieldGroup(self, selection_group):
        """ This method has been extended to also update the 'node number' field for determining the displayed number
        during the selection. Only should occur during Element Creation Mode
        :param selection_group: selection_group is a FieldGroup that stores the selected graphics
        :return: None
        """
        self._scenepicker.addPickedNodesToFieldGroup(selection_group)

    def setIgnoreMouseEvents(self, value):
        self._ignore_mouse_events = value

    def getVectorDelta(self, nodeEditInfo, x, y):
        """
        Get the delta of position between selected nodes and provided windows coordinates
        """
        delta = [0.0, 0.0, 0.0]
        fieldmodule = nodeEditInfo._node.getNodeset().getFieldmodule()
        fieldcache = fieldmodule.createFieldcache()
        fieldcache.setNode(nodeEditInfo._node)
        numberOfComponents = nodeEditInfo._orientationField.getNumberOfComponents()

        if numberOfComponents > 0 and numberOfComponents <= 9 and nodeEditInfo._orientationField.isValid() and \
                nodeEditInfo._coordinateField.isValid() and nodeEditInfo._glyphScaleFactors[0] != 0.0 and \
                        nodeEditInfo._glyphCentre[0] == 0.0 and nodeEditInfo._glyphSize[0] == 0.0 and \
                not nodeEditInfo._variableScaleField.isValid():
            return_code, orientation = nodeEditInfo._orientationField.evaluateReal(fieldcache, numberOfComponents)
            return_code, coordinates = nodeEditInfo._coordinateField.evaluateReal(fieldcache, 3)
            axis1, axis2, axis3, num = zinc_static.makeGlyphOrientationScaleAxes(orientation)
            oldCoordinates = [coordinates[0], coordinates[1], coordinates[2]]
            endCoordinates = [0.0, 0.0, 0.0]
            endCoordinates[0] = coordinates[0]+nodeEditInfo._glyphSize[0]*nodeEditInfo._glyphScaleFactors[0]*axis1[0]
            endCoordinates[1] = coordinates[1]+nodeEditInfo._glyphSize[0]*nodeEditInfo._glyphScaleFactors[0]*axis1[1]
            endCoordinates[2] = coordinates[2]+nodeEditInfo._glyphSize[0]*nodeEditInfo._glyphScaleFactors[0]*axis1[2]
            projectCoordinates = self._zincwidget.project(endCoordinates[0], endCoordinates[1], endCoordinates[2])
            endCoordinates = self._zincwidget.unproject(x, y * -1.0, projectCoordinates[2])
            a = [0.0, 0.0, 0.0]
            a[0] = (endCoordinates[0]-oldCoordinates[0])/nodeEditInfo._glyphScaleFactors[0]
            a[1] = (endCoordinates[1]-oldCoordinates[1])/nodeEditInfo._glyphScaleFactors[0]
            a[2] = (endCoordinates[2]-oldCoordinates[2])/nodeEditInfo._glyphScaleFactors[0]
            if numberOfComponents == 1:
                delta[0] = a[0]
            elif numberOfComponents == 2 or numberOfComponents == 4:
                delta[0] = a[0]
                delta[1] = a[1]
            elif numberOfComponents == 3 or numberOfComponents == 6 or numberOfComponents == 9:
                delta[0] = a[0]
                delta[1] = a[1]
                delta[2] = a[2]

        return delta

    def getPlacementPoint(self, nodeEditInfo, x, y):
        """
        Return the world coordinates with the informations provided in nodeEditInfo
        """
        if not nodeEditInfo._nearestElement:
            if nodeEditInfo._node and nodeEditInfo._node.isValid():
                fieldmodule = nodeEditInfo._node.getNodeset().getFieldmodule()
                fieldcache = fieldmodule.createFieldcache()
                fieldcache.setNode(nodeEditInfo._node)
                return_code, coordinates = nodeEditInfo._coordinateField.evaluateReal(fieldcache, 3)
                projectCoordinates = self._zincwidget.project(coordinates[0], coordinates[1], coordinates[2])
                unprojectCoordinates = self._zincwidget.unproject(x, y * -1.0, projectCoordinates[2])
                return True, unprojectCoordinates
            else:
                return self._scenepicker.getPickingVolumeCentre()
        else:
            fieldmodule = nodeEditInfo._nearestElement.getMesh().getFieldmodule()
            fieldcache = fieldmodule.createFieldcache()
            converged = False
            fieldcache.setMeshLocation(nodeEditInfo._nearestElement, [0.5, 0.5, 0.5])
            temp, unprojectCoordinates = self._scenepicker.getPickingVolumeCentre()
            fieldcache.clearLocation()
            mesh = nodeEditInfo._nearestElement.getMesh()
            fieldmodule = nodeEditInfo._elementCoordinateField.getFieldmodule()
            meshGroup = fieldmodule.createFieldGroup().createFieldElementGroup(mesh).getMeshGroup()
            meshGroup.addElement(nodeEditInfo._nearestElement)
            return_code = True
            steps = 0
            point = unprojectCoordinates
            while return_code and not converged:
                previous_point = point
                temp, fe_value_point = zinc_static.elementConstrainFunction(fieldmodule,
                                                                            fieldcache,
                                                                            previous_point,
                                                                            nodeEditInfo._elementCoordinateField,
                                                                            meshGroup)
                changes = [point[0] - fe_value_point[0], point[1] - fe_value_point[1],
                           point[2] - fe_value_point[2]]
                if math.sqrt(changes[0]*changes[0] + changes[1]*changes[1] + changes[2]*changes[2]) < 1.0e-4:
                    converged = True
                else:
                    point[0] = fe_value_point[0]
                    point[1] = fe_value_point[1]
                    point[2] = fe_value_point[2]
                    steps += 1
                    if steps > 1000:
                        return_code = False
                    projectCoordinates = self._zincwidget.project(point[0], point[1], point[2])
                    point = self._zincwidget.unproject(x * 1.0, y * -1.0, projectCoordinates[2])
                    changes = [point[0] - previous_point[0], point[1] - previous_point[1],
                               point[2] - previous_point[2]]
                    if math.sqrt(changes[0]*changes[0] + changes[1]*changes[1] + changes[2]*changes[2]) < 1.0e-6:
                        return_code = False
            return True, point

    def getCoordinatesDelta(self, nodeEditInfo, x, y):
        """
        Get the delta of position between selected nodes and provided windows coordinates
        """
        fieldmodule = nodeEditInfo._node.getNodeset().getFieldmodule()
        fieldcache = fieldmodule.createFieldcache()
        fieldcache.setNode(nodeEditInfo._node)
        delta = [0.0, 0.0, 0.0]
        return_code, coordinates = nodeEditInfo._coordinateField.evaluateReal(fieldcache, 3)
        return_code, newCoordinates = self.getPlacementPoint(nodeEditInfo, x, y)
        if return_code:
            delta = [newCoordinates[0] - coordinates[0],  newCoordinates[1] - coordinates[1],
                     newCoordinates[2] - coordinates[2]]
        return delta

    def updateSelectedNodesCoordinatesWithDelta(self, nodeEditInfo, selectionGroup, xdiff, ydiff, zdiff):
        """
        Updated nodes in the selection group with delta
        """
        nodegroup = selectionGroup.getFieldNodeGroup(nodeEditInfo._node.getNodeset())
        if nodegroup.isValid():
            group = nodegroup.getNodesetGroup()
            if group.isValid():
                fieldmodule = nodegroup.getFieldmodule()
                fieldmodule.beginChange()
                fieldcahce = fieldmodule.createFieldcache()
                iterator = group.createNodeiterator()
                node = iterator.next()
                while node.isValid():
                    zinc_static.updateNodePositionWithDelta(fieldcahce, nodeEditInfo._coordinateField,
                                                            node, xdiff, ydiff, zdiff)
                    node = iterator.next()
                fieldmodule.endChange()

        # Sums up the total x, y and z displacements to be inverted later.
        self._nodeMove = [A + B for A, B in zip((xdiff, ydiff, zdiff), self._nodeMove)]

    def nodeIsSelectedAtCoordinates(self, x, y):
        """
        Return node and its coordinates field if its valid
        """
        self._scenepicker.setSceneviewerRectangle(self._sceneviewer, SCENECOORDINATESYSTEM_LOCAL,
                                                  x - 3, y - 3,
                                                  x + 3, y + 3)
        node = self._scenepicker.getNearestNode()
        if node.isValid():
            nodeset = node.getNodeset()
            nodegroup = self._selectionGroup.getFieldNodeGroup(nodeset)
            if nodegroup.isValid():
                group = nodegroup.getNodesetGroup()
                if group.containsNode(node):
                    graphics = self._scenepicker.getNearestGraphics()
                    if graphics.getFieldDomainType() == Field.DOMAIN_TYPE_NODES:
                        return True, node, graphics.getCoordinateField(), graphics
        return False, None, None, None

    def getNearestSurfacesElementAndCoordinates(self, x, y):
        """
        Return element and its coordinates field if its valid
        """
        self._scenepicker.setSceneviewerRectangle(self._sceneviewer, SCENECOORDINATESYSTEM_LOCAL,
                                                  x - 3, y - 3, x + 3, y + 3)
        nearestGraphics = self._scenepicker.getNearestGraphics()
        if nearestGraphics.isValid():
            surfacesGraphics = nearestGraphics.castSurfaces()
            if surfacesGraphics.isValid():
                nearestElement = self._scenepicker.getNearestElement()
                elementCoordinateField = surfacesGraphics.getCoordinateField()
                return True, nearestElement, elementCoordinateField
        return False, None, None

    def createNodeAtCoordinates(self, nodeEditInfo, x, y):
        """
        Create a new node at a location based on information provided by nodeEditInfo
        Also adds a delete to the Undo List
        """
        return_code, newCoordinates = self.getPlacementPoint(nodeEditInfo, x, y)
        if nodeEditInfo._createCoordinatesField and nodeEditInfo._createCoordinatesField.isValid():
            fieldmodule = nodeEditInfo._createCoordinatesField.getFieldmodule()
            fieldmodule.beginChange()
            nodeset = fieldmodule.findNodesetByFieldDomainType(Field.DOMAIN_TYPE_NODES)

            nodetemplate = nodeset.createNodetemplate()
            nodetemplate.defineField(nodeEditInfo._createCoordinatesField)
            node = nodeset.createNode(-1, nodetemplate)
            fieldcache = fieldmodule.createFieldcache()
            fieldcache.setNode(node)

            # Thomas edit
            nodeset_field_group = fieldmodule.createFieldNodeGroup(nodeset)
            nodeset_field_group.getNodesetGroup().addNode(node)
            self._undo.addNode(nodeset_field_group)
            self._undo.addAction('delete node', nodeset_field_group)

            nodeEditInfo._createCoordinatesField.assignReal(fieldcache, newCoordinates)
            fieldmodule.endChange()

    def proceedSceneViewerMousePressEvent(self, event):
        """
        Inform the scene viewer of a mouse press event.
        """
        # TODO remove the bandboxing when selecting Elements
        self._handle_mouse_events = False  # Track when the zinc should be handling mouse events
        if not self._ignore_mouse_events and (event.modifiers() & self._selectionModifier) and \
                (self._nodeSelectMode or self._elemSelectMode) \
                and button_map[event.button()] == Sceneviewerinput.BUTTON_TYPE_LEFT:
            self._selection_position_start = (event.x(), event.y())
            self._selection_mode = SelectionMode.EXCLUSIVE
            if event.modifiers() & self._additiveSelectionModifier or self._selectionAlwaysAdditive:
                self._selection_mode = SelectionMode.ADDITIVE
        elif not self._ignore_mouse_events and (event.modifiers() & self._editModifier) and \
                (self._nodeEditMode or
                     self._nodeEditVectorMode or
                     self._nodeCreateMode or
                     self._nodeDeleteMode or
                     self._elemCreateMode) \
                and button_map[event.button()] == Sceneviewerinput.BUTTON_TYPE_LEFT:

            return_code, selectedNode, selectedCoordinateField, selectedGraphics = \
                self.nodeIsSelectedAtCoordinates(event.x(), event.y())
            if return_code:
                self._nodeEditInfo._node = selectedNode
                self._nodeEditInfo._coordinateField = selectedCoordinateField
                self._nodeEditInfo._graphics = selectedGraphics
                self._selection_position_start = (event.x(), event.y())
                if self._nodeEditMode:
                    self._selection_mode = SelectionMode.EDIT_POSITION
                    self._nodeMove = (0, 0, 0)  # Reset the movement of the nodes
                    nodegroup = self._selectionGroup.getFieldNodeGroup(selectedNode.getNodeset()).getNodesetGroup()
                    # iterator = self._nodeset_group.createNodeiterator()
                    iterator = nodegroup.createNodeiterator()
                    node = iterator.next()

                    while node.isValid():
                        self._nodesMoved.append(node.getIdentifier())  # A list containing all the nodes that are currently selected.
                        node = iterator.next()

                else:
                    attributes = self._nodeEditInfo._graphics.getGraphicspointattributes()
                    if attributes.isValid():
                        self._nodeEditInfo._orientationField = attributes.getOrientationScaleField()
                        if self._nodeEditInfo._orientationField and self._nodeEditInfo._orientationField.isValid():
                            self._selection_mode = SelectionMode.EDIT_VECTOR
                            return_code, self._nodeEditInfo._glyphCentre = attributes.getGlyphOffset(3)
                            return_code, self._nodeEditInfo._glyphSize = attributes.getBaseSize(3)
                            return_code, self._nodeEditInfo._glyphScaleFactors = attributes.getScaleFactors(3)
                            self._nodeEditInfo._variableScaleField = attributes.getSignedScaleField()
            elif self._nodeCreateMode:
                self._selection_mode = SelectionMode.CREATE
        elif not self._ignore_mouse_events and not event.modifiers() or (event.modifiers() & self._selectionModifier and button_map[event.button()] == Sceneviewerinput.BUTTON_TYPE_RIGHT):
            scene_input = self._sceneviewer.createSceneviewerinput()
            scene_input.setPosition(event.x(), event.y())
            scene_input.setEventType(Sceneviewerinput.EVENT_TYPE_BUTTON_PRESS)
            scene_input.setButtonType(button_map[event.button()])
            scene_input.setModifierFlags(modifier_map(event.modifiers()))

            self._sceneviewer.processSceneviewerinput(scene_input)

            self._handle_mouse_events = True
        else:
            event.ignore()

    def proceedSceneViewerMouseReleaseEvent(self, event):
        """
        Inform the scene viewer of a mouse release event.
        """
        if not self._ignore_mouse_events and (self._selection_mode == SelectionMode.EDIT_POSITION or
                                                      self._selection_mode == SelectionMode.EDIT_VECTOR):
            self._nodeEditInfo.reset()
            self._undo.addAction('move node', (self._nodeMove, self._nodesMoved))
            self._nodesMoved = []
            self.update_volume.emit()
        elif not self._ignore_mouse_events and self._selection_mode == SelectionMode.CREATE:
            x = event.x()
            y = event.y()
            self._selection_box.setVisibilityFlag(False)
            if self._createCoordinatesField and self._createCoordinatesField.isValid():
                self._nodeEditInfo._createCoordinatesField = self._createCoordinatesField
                if self._nodeConstrainMode:
                    returnCode, self._nodeEditInfo._nearestElement, self._nodeEditInfo._elementCoordinateField = \
                        self.getNearestSurfacesElementAndCoordinates(x, y)
                    if self._nodeEditInfo._nearestElement and self._nodeEditInfo._elementCoordinateField:
                        self.createNodeAtCoordinates(self._nodeEditInfo, x, y)
                else:
                    self.createNodeAtCoordinates(self._nodeEditInfo, x, y)
            self._nodeEditInfo.reset()
        elif not self._ignore_mouse_events and self._selection_mode != SelectionMode.NONE:
            x = event.x()
            y = event.y()
            # Construct a small frustum to look for nodes in.
            top_region = self._sceneviewer.getScene().getRegion()
            top_region.beginHierarchicalChange()
            self._selection_box.setVisibilityFlag(False)
            if x != self._selection_position_start[0] and y != self._selection_position_start[1]:
                # Disabled the "bandbox" when creating nodes or elements
                if self._nodeCreateMode or self._elemCreateMode:
                    event.ignore()
                    return
                left = min(x, self._selection_position_start[0])
                right = max(x, self._selection_position_start[0])
                bottom = min(y, self._selection_position_start[1])
                top = max(y, self._selection_position_start[1])
                self._scenepicker.setSceneviewerRectangle(
                    self._sceneviewer, SCENECOORDINATESYSTEM_LOCAL, left, bottom, right, top)
                if self._selection_mode == SelectionMode.EXCLUSIVE:
                    self._selectionGroup.clear()
                if self._nodeSelectMode or self._dataSelectMode:
                    self.addPickedNodesToFieldGroup(self._selectionGroup)
                # Removes nodes when in Delete Mode
                if self._nodeDeleteMode:
                    NodesetGroupField = self._selectionGroup.getFieldNodeGroup(self._nodeset_group)
                    self._undo.addAction('create node', NodesetGroupField)
                    self._undo.removeNode(NodesetGroupField)
                if self._elemSelectMode:
                    self._scenepicker.addPickedElementsToFieldGroup(self._selectionGroup)
            else:
                self._scenepicker.setSceneviewerRectangle(self._sceneviewer, SCENECOORDINATESYSTEM_LOCAL, x - 3.5, y - 3.5, x + 3.5, y + 3.5)
                if self._nodeSelectMode and self._elemSelectMode and self._selection_mode == SelectionMode.EXCLUSIVE and not self._scenepicker.getNearestGraphics().isValid():
                    self._selectionGroup.clear()

                if self._nodeSelectMode and \
                        (self._scenepicker.getNearestGraphics().getFieldDomainType() == Field.DOMAIN_TYPE_NODES):
                    node = self._scenepicker.getNearestNode()
                    nodeset = node.getNodeset()
                    nodegroup = self._selectionGroup.getFieldNodeGroup(nodeset)
                    if not nodegroup.isValid():
                        nodegroup = self._selectionGroup.createFieldNodeGroup(nodeset)
                    group = nodegroup.getNodesetGroup()
                    if self._selection_mode == SelectionMode.EXCLUSIVE:
                        remove_current = group.getSize() == 1 and group.containsNode(node)
                        self._selectionGroup.clear()
                        if not remove_current:
                            group.addNode(node)
                    elif self._selection_mode == SelectionMode.ADDITIVE:

                        # Thomas edits to change the node number field on selected nodes.
                        nodetemplate = nodeset.createNodetemplate()
                        with get_field_module(self._createCoordinatesField) as field_module:
                            numberField = field_module.findFieldByName('node_number')
                            if group.containsNode(node):
                                group.removeNode(node)
                                nodetemplate.undefineField(numberField)
                                self._selected_nodes.remove(node.getIdentifier())

                            else:
                                group.addNode(node)
                                nodetemplate.defineField(numberField)
                                self._selected_nodes.append(node.getIdentifier())
                                if len(self._selected_nodes) == 4:
                                    self.create2DFiniteElementfromSelection()

                            node.merge(nodetemplate)
                            field_cache = field_module.createFieldcache()
                            # Update the labels
                            for i, node_iden in enumerate(self._selected_nodes):
                                n = nodeset.findNodeByIdentifier(node_iden)
                                field_cache.setNode(n)
                                numberField.assignString(field_cache, str(i + 1))
                    # Removes the selected node or nodes
                    if self._nodeDeleteMode:
                        NodesetGroupField = self._selectionGroup.getFieldNodeGroup(self._nodeset_group)
                        self._undo.addAction('create node', NodesetGroupField)
                        self._undo.removeNode(NodesetGroupField)
                if self._elemSelectMode and \
                        (self._scenepicker.getNearestGraphics().getFieldDomainType() in
                             [Field.DOMAIN_TYPE_MESH1D,
                              Field.DOMAIN_TYPE_MESH2D,
                              Field.DOMAIN_TYPE_MESH3D,
                              Field.DOMAIN_TYPE_MESH_HIGHEST_DIMENSION]):
                    elem = self._scenepicker.getNearestElement()
                    mesh = elem.getMesh()
                    elementgroup = self._selectionGroup.getFieldElementGroup(mesh)
                    if not elementgroup.isValid():
                        elementgroup = self._selectionGroup.createFieldElementGroup(mesh)
                    group = elementgroup.getMeshGroup()
                    if self._selection_mode == SelectionMode.EXCLUSIVE:
                        remove_current = group.getSize() == 1 and group.containsElement(elem)
                        self._selectionGroup.clear()
                        if not remove_current:
                            group.addElement(elem)
                    elif self._selection_mode == SelectionMode.ADDITIVE:
                        if group.containsElement(elem):
                            group.removeElement(elem)
                        else:
                            group.addElement(elem)
            top_region.endHierarchicalChange()
            self._selection_mode = SelectionMode.NONE
        elif not self._ignore_mouse_events and self._handle_mouse_events:
            scene_input = self._sceneviewer.createSceneviewerinput()
            scene_input.setPosition(event.x(), event.y())
            scene_input.setEventType(Sceneviewerinput.EVENT_TYPE_BUTTON_RELEASE)
            scene_input.setButtonType(button_map[event.button()])

            self._sceneviewer.processSceneviewerinput(scene_input)
        else:
            event.ignore()
        self._selection_mode = SelectionMode.NONE

    def proceedSceneViewerMouseMoveEvent(self, event):
        """
        Inform the scene viewer of a mouse move event and update the OpenGL scene to reflect this
        change to the viewport.
        """

        if not self._ignore_mouse_events and self._selection_mode != SelectionMode.NONE:

            # Thomas addition
            if self._selection_position_start is None:
                event.ignore()
                return

            x = event.x()
            y = event.y()
            xdiff = float(x - self._selection_position_start[0])
            ydiff = float(y - self._selection_position_start[1])
            if abs(xdiff) < 0.0001:
                xdiff = 1
            if abs(ydiff) < 0.0001:
                ydiff = 1
            if self._selection_mode == SelectionMode.EDIT_POSITION:
                if self._nodeConstrainMode:
                    returnCode, self._nodeEditInfo._nearestElement, self._nodeEditInfo._elementCoordinateField = \
                        self.getNearestSurfacesElementAndCoordinates(x, y)
                    if self._nodeEditInfo._nearestElement and self._nodeEditInfo._elementCoordinateField:
                        delta = self.getCoordinatesDelta(self._nodeEditInfo, x, y)
                        self.updateSelectedNodesCoordinatesWithDelta(self._nodeEditInfo,
                                                                     self._selectionGroup, delta[0], delta[1], delta[2])
                else:
                    delta = self.getCoordinatesDelta(self._nodeEditInfo, x, y)
                    self.updateSelectedNodesCoordinatesWithDelta(self._nodeEditInfo,
                                                                 self._selectionGroup, delta[0], delta[1], delta[2])
            elif self._selection_mode == SelectionMode.EDIT_VECTOR:
                if self._nodeEditInfo._orientationField and self._nodeEditInfo._orientationField.isValid():
                    delta = self.getVectorDelta(self._nodeEditInfo, x, y)
                    zinc_static.updateSelectedNodesVectorWithDelta(self._nodeEditInfo,
                                                                   self._selectionGroup, delta[0], delta[1], delta[2])
            else:
                # Disabled the "bandbox" when creating nodes or elements
                if self._elemCreateMode or self._nodeCreateMode:
                    event.ignore()
                    return
                xoff = float(self._selection_position_start[0]) / xdiff + 0.5
                yoff = float(self._selection_position_start[1]) / ydiff + 0.5
                scene = self._selection_box.getScene()
                scene.beginChange()
                self._selectionBox_setBaseSize([xdiff, ydiff, 0.999])
                self._selectionBox_setGlyphOffset([xoff, -yoff, 0])
                self._selection_box.setVisibilityFlag(True)
                scene.endChange()
        elif not self._ignore_mouse_events and self._handle_mouse_events:
            scene_input = self._sceneviewer.createSceneviewerinput()
            scene_input.setPosition(event.x(), event.y())
            scene_input.setEventType(Sceneviewerinput.EVENT_TYPE_MOTION_NOTIFY)
            if event.type() == QtCore.QEvent.Leave:
                scene_input.setPosition(-1, -1)
            self._sceneviewer.processSceneviewerinput(scene_input)
        else:
            event.ignore()

    def setCreatedNodeGroup(self, nodeGroup):
        self._nodeset_group = nodeGroup

    def getCreateNodeGroup(self):
        if self._nodeset_group is not None:
            return self._nodeset_group
        else:
            raise AttributeError

    def clearSelection(self):
        """
        Clears the selection group along with the labels defined on it.
        :return: None
        """
        with get_field_module(self._createCoordinatesField) as field_module:
            node_number = field_module.findFieldByName('node_number')
            nodeset = field_module.findNodesetByFieldDomainType(Field.DOMAIN_TYPE_NODES)
            nodetemplate = nodeset.createNodetemplate()
            nodetemplate.undefineField(node_number)

            for n in self._selected_nodes:
                node = nodeset.findNodeByIdentifier(n)
                node.merge(nodetemplate)

        self._selected_nodes = []
        self._selectionGroup.clear()

    def getSelectedNodes(self):
        """
        This method returns the coordinates of all selected nodes in a certain region
        :return: An array of the Coordinates else None
        """

        NodesetGroup = self._selectionGroup.getFieldNodeGroup(self._nodeset_group).getNodesetGroup()

        NodeIter = NodesetGroup.createNodeiterator()
        node = NodeIter.next()

        Values = []

        while node.isValid():
            Values.append(node)
            node = NodeIter.next()

        return Values

    def undoLastAction(self):
        self._undo.undo()

    def redoLastAction(self):
        self._undo.redo()

    def create2DFiniteElementfromSelection(self):
        """
        Create a 2D finite element from the field_module, finite_element_field and node_coordinates.
        Uses attributes for the node index order and createCoordinate field. Assumes that this will only
        happen when there are 4 nodes selected.
        """
        # Find a special node set named 'cmiss_nodes'
        field_module = self._createCoordinatesField.getFieldmodule()
        nodeset = field_module.findNodesetByName('nodes')
        node_template = nodeset.createNodetemplate()
        finite_element_field = self._createCoordinatesField

        # Set the finite element coordinate field for the nodes to use
        node_template.defineField(finite_element_field)

        mesh = field_module.findMeshByDimension(2)
        element_template = mesh.createElementtemplate()
        element_template.setElementShapeType(Element.SHAPE_TYPE_SQUARE)
        element_node_count = 4
        element_template.setNumberOfNodes(element_node_count)

        linear_basis = field_module.createElementbasis(2, Elementbasis.FUNCTION_TYPE_LINEAR_LAGRANGE)
        node_indexes = [1, 2, 3, 4]
        element_template.defineFieldSimpleNodal(finite_element_field, -1, linear_basis, node_indexes)

        for i, node_identifier in enumerate(self._selected_nodes):
            node = nodeset.findNodeByIdentifier(node_identifier)
            element_template.setNode(i+1, node)

        mesh.defineElement(-1, element_template)
        self.clearSelection()
