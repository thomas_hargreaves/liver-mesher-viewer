import contextlib


@contextlib.contextmanager
def get_scene(region):
    """
    A context manager for the Scene class. When used with the Python
    with statement ensures that beginChange() and endChange() are called.
    Usage:
        with get_scene(region) as scene:
            scene.some_method()
    """
    scene = region.getScene()
    scene.beginChange()
    try:
        yield scene
    finally:
        scene.endChange()
    return


@contextlib.contextmanager
def get_material_module(Class):
    """
    A context manager for the Materialmodule class. When used with the Python
    with statement ensures that beginChange() and endChange() are called.
    Can be called from the region or context
    Usage:
        with get_material_module(region) as scene:
            material_module.some_method()
    """
    try:
        mm = Class.getMaterialmodule()
    except AttributeError:
        mm = Class.getScene().getMaterialmodule()
    mm.beginChange()
    try:
        yield mm
    finally:
        mm.endChange()


@contextlib.contextmanager
def get_field_module(region):
    """
    A context manager for the Fieldmodule class. When used with the Python
    with statement ensures that beginChange() and endChange() are called.
    Usage:
        with get_field_module(region) as field_module:
            field_module.some_method()
    """
    fm = region.getFieldmodule()
    fm.beginChange()
    try:
        yield fm
    finally:
        fm.endChange()
    return


@contextlib.contextmanager
def get_scene_filter_module(scene):
    sfm = scene.getScenefiltermodule()
    sfm.beginChange()
    try:
        yield sfm
    finally:
        sfm.endChange()
    return

