from opencmiss.zinc.element import Element
from zinc_helper import *


class zincVolume(object):
    def __init__(self):
        self._volume = {}
        self._BBMesh = None
        self._volMesh = None
        self._region = None
        self._coordinateField = None

    def setCoordinateField(self, coordinateField):
        self._coordinateField = coordinateField

    def setRegion(self, region):
        self._region = region

    def setMeshes(self, BBMesh, volMesh):
        self._BBMesh = BBMesh
        self._volMesh = volMesh

    def calculateVolume(self):
        """
        This function calculates the volume. To do optionally in the future
        it to check for field changes that might speed the computation up.
        Currently it just recalculated the volume in each section
        :return: A Dictionary containing the volumes
        """
        region = self._region
        # Get the field module
        with get_field_module(region) as field_module:

            co_ordinates = self._coordinateField
            # Create the field Cache and get the 2 meshes
            liver_mesh = self._volMesh
            bb_mesh = self._BBMesh
            cache = field_module.createFieldcache()

            volume_total = 0

            total = field_module.findFieldByName('Volume_Total')
            if total.isValid():

                result, self._volume["Total"] = total.evaluateReal(cache, 1)
                for i in range(1, 9):
                    result, self._volume[i] = field_module.findFieldByName("Volume_{}".format(i)).evaluateReal(cache, 1)

            else:

                element_iter = bb_mesh.createElementiterator()
                element = element_iter.next()

                for i in range(1, 9):

                    FG = field_module.createFieldElementGroup(bb_mesh)
                    # FG.setName("Section" + str(i))
                    mesh_group = FG.getMeshGroup()
                    mesh_group.removeAllElements()
                    mesh_group.addElement(element)
                    element = element_iter.next()

                    integrand_field = field_module.createFieldFindMeshLocation(co_ordinates, co_ordinates, mesh_group)
                    integrand_field.setSearchMode(integrand_field.SEARCH_MODE_EXACT)
                    is_defined = field_module.createFieldIsDefined(integrand_field)

                    volume_field = field_module.createFieldMeshIntegral(is_defined, co_ordinates, liver_mesh)
                    volume_field.setElementQuadratureRule(Element.QUADRATURE_RULE_GAUSSIAN)
                    volume_field.setNumbersOfPoints([3, 3, 3])
                    volume_field.setManaged(True)
                    volume_field.setName("Volume_{}".format(i))
                    result, volume = volume_field.evaluateReal(cache, 1)
                    volume_total += volume
                    self._volume[i] = volume

                ones = field_module.createFieldConstant(1.0)
                volume_field = field_module.createFieldMeshIntegral(ones, co_ordinates, liver_mesh)
                volume_field.setElementQuadratureRule(Element.QUADRATURE_RULE_GAUSSIAN)
                volume_field.setNumbersOfPoints([3, 3, 3])
                volume_field.setManaged(True)
                volume_field.setName("Volume_Total")
                result, self._volume["Total"] = volume_field.evaluateReal(cache, 1)

        return self._volume

