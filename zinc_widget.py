try:
    from PySide import QtCore, QtOpenGL
except ImportError:
    from PyQt4 import QtCore, QtOpenGL

from opencmiss.zinc.glyph import Glyph
from opencmiss.zinc.sceneviewer import Sceneviewer, Sceneviewerevent
from opencmiss.zinc.sceneviewerinput import Sceneviewerinput
from opencmiss.zinc.scenecoordinatesystem import \
    SCENECOORDINATESYSTEM_WINDOW_PIXEL_TOP_LEFT, \
    SCENECOORDINATESYSTEM_WORLD
from opencmiss.zinc.element import Element, Elementbasis
from opencmiss.zinc.field import Field, FieldImage, FieldFindMeshLocation
from opencmiss.zinc.optimisation import Optimisation
from opencmiss.zinc.node import Node
from opencmiss.zinc.status import *
from opencmiss.zinc.material import Material

from zinc_interactive_tool import ZincInteractiveTool
from zinc_helper import *
import zinc_interactive_static as zinc_static
import zinc_volume

import itertools
import numpy as np
import math
import os.path

# mapping from qt to zinc start
# Create a button map of Qt mouse buttons to Zinc input buttons
button_map = {QtCore.Qt.LeftButton: Sceneviewerinput.BUTTON_TYPE_LEFT,
              QtCore.Qt.MidButton: Sceneviewerinput.BUTTON_TYPE_MIDDLE,
              QtCore.Qt.RightButton: Sceneviewerinput.BUTTON_TYPE_RIGHT}


def modifier_map(qt_modifiers):
    """
    Return a Zinc SceneViewerInput modifiers object that is created from
    the Qt modifier flags passed in.
    """
    modifiers = Sceneviewerinput.MODIFIER_FLAG_NONE
    if qt_modifiers & QtCore.Qt.SHIFT:
        modifiers = modifiers | Sceneviewerinput.MODIFIER_FLAG_SHIFT

    return modifiers
# mapping from qt to zinc end

SELECTION_RUBBERBAND_NAME = 'selection_rubberband'


# projectionMode start
class ProjectionMode(object):

    PARALLEL = 0
    PERSPECTIVE = 1
# projectionMode end


class ZincWidget(QtOpenGL.QGLWidget):
    """
    This widget can displays 3D rendered graphics using the Open CMISS library Zinc.
    It is plugged into the User Interface.
    """

    QtCore.pyqtSignal = QtCore.Signal
    QtCore.pyqtSlot = QtCore.Slot

    # Signals
    # update_volume = QtCore.Signal(dict)
    update_slider = QtCore.Signal('int')
    graphicsInitialized = QtCore.Signal()

    # init start
    def __init__(self, parent=None, shared=None):
        """
        Call the super class init functions, set the  Zinc context and the scene viewer handle to None.
        Initialise other attributes that deal with selection and the rotation of the plane.
        """
        super(ZincWidget, self).__init__(parent, shared)
        # Create a Zinc context from which all other objects can be derived either directly or indirectly.
        self._context = None
        self._scene_viewer = None
        self._scene_picker = None
        self._volumeTool = None
        self._interactiveTool = ZincInteractiveTool(self)

        # Liver based variables
        self._current_image = None
        self._region = None
        self._scene_viewer_notifier = None
        self._current_image_set = None
        self._image_count = None
        self._image = None

        # Coordinate Fields
        self._window_coords_from = None
        self._window_coords_to = None
        self._global_coords_from = None
        self._global_coords_to = None

        # Flags
        self._flag_node_none = False
        self._flag_node_all = False
        self._flag_image_none = False
        self._flag_image_all = False
        self._flag_isolines = False
        self._flag_mesh_solid = False
        self._flag_mesh_trans = False

    def setFlag(self, flag, value, option=None):
        if option is not None:
            name = '_flag_{}_{}'.format(flag, option)
        else:
            name = '_flag_{}'.format(flag)
        try:
            setattr(self, name, value)
        except AttributeError:
            print 'Error flag does not exist'

    def setContext(self, context):
        """
        Sets the context for this ZincWidget.  This should be set before the initializeGL()
        method is called otherwise the scene viewer cannot be created.
        """
        self._context = context
        self._region = self._context.getDefaultRegion()

    def getContext(self):
        """
        Returns the current Zinc Context
        """
        if self._context is None:
            raise RuntimeError("Zinc context has not been set.")
        return self._context

    def getSceneViewer(self):
        """
        Get the scene viewer for this ZincWidget.
        """
        return self._scene_viewer

    def getScenePicker(self):
        """
        Get the scene picker if set.
        :return: ScenePicker
        """
        return self._interactiveTool.getScenepicker()

    def setCurrentImageSet(self, set, count=None):
        self._current_image_set = set
        if count is not None:
            self._image_count = count

    def getCurrentImageSet(self):
        return self._current_image_set

    def initializeGL(self):
        """
        Initialise the Zinc scene for drawing the axis glyph at a point.
        """

        if self._scene_viewer is None:

            scene_viewer_module = self._context.getSceneviewermodule()
            self._scene_viewer = scene_viewer_module.createSceneviewer(
                Sceneviewer.BUFFERING_MODE_DOUBLE, Sceneviewer.STEREO_MODE_DEFAULT)
            self._scene_viewer.setProjectionMode(Sceneviewer.PROJECTION_MODE_PERSPECTIVE)
            self._scene_viewer.setTransparencyMode(Sceneviewer.TRANSPARENCY_MODE_SLOW)
            # self._scene_viewer.setTransparencyMode(Sceneviewer.TRANSPARENCY_MODE_ORDER_INDEPENDENT)
            # self._scene_viewer.setTransparencyLayers(5)

            filter_module = self._context.getScenefiltermodule()
            graphics_filter = filter_module.createScenefilterVisibilityFlags()

            self._scene_viewer.setScenefilter(graphics_filter)

            region = self._region

            with get_field_module(region) as field_module:
                self._window_coords_from = field_module.createFieldConstant([0, 0, 0])
                self._global_coords_from = field_module.createFieldConstant([0, 0, 0])
                unproject = field_module.createFieldSceneviewerProjection(
                    self._scene_viewer, SCENECOORDINATESYSTEM_WINDOW_PIXEL_TOP_LEFT, SCENECOORDINATESYSTEM_WORLD)
                project = field_module.createFieldSceneviewerProjection(
                    self._scene_viewer, SCENECOORDINATESYSTEM_WORLD, SCENECOORDINATESYSTEM_WINDOW_PIXEL_TOP_LEFT)

            self._global_coords_to = field_module.createFieldProjection(self._window_coords_from, unproject)
            self._window_coords_to = field_module.createFieldProjection(self._global_coords_from, project)

            with get_scene(region) as scene:
                self._scene_viewer.setScene(scene)

            self._scene_viewer_notifier = self._scene_viewer.createSceneviewernotifier()
            self._scene_viewer_notifier.setCallback(self._zincSceneviewerEvent)

            self._interactiveTool.setSceneviewer(self._scene_viewer)

            # self.initializeTool()
            self.viewImage(0)
            self.viewAll()
            # initializeGL end

    def initializeTool(self):

        region = self._region
        if not region.isValid():
            return False
        with get_field_module(region) as field_module:
            finite_element_field = field_module.findFieldByName('coordinates')
            self._interactiveTool.setNodeConstrainToSurfacesMode(True)
            self._interactiveTool.setNodeCreateCoordinatesField(finite_element_field)
            nodeset = field_module.findNodesetByName('node_cloud.nodes').castGroup()

            self._interactiveTool.setCreatedNodeGroup(nodeset)
            self._interactiveTool.setNodeCreateMode(True)
            self._interactiveTool.setNodeSelection(False)
            self._interactiveTool.setNodeEdit(False)
            self._interactiveTool.setNodeDeleteMode(False)
            self._interactiveTool.setSelectionModeAdditive(False)

    def setProjectionMode(self, mode):
        if mode == ProjectionMode.PARALLEL:
            self._scene_viewer.setProjectionMode(Sceneviewer.PROJECTION_MODE_PARALLEL)
        elif mode == ProjectionMode.PERSPECTIVE:
            self._scene_viewer.setProjectionMode(Sceneviewer.PROJECTION_MODE_PERSPECTIVE)

    def getProjectionMode(self):
        if self._scene_viewer.getProjectionMode() == Sceneviewer.PROJECTION_MODE_PARALLEL:
            return ProjectionMode.PARALLEL
        elif self._scene_viewer.getProjectionMode() == Sceneviewer.PROJECTION_MODE_PERSPECTIVE:
            return ProjectionMode.PERSPECTIVE

    def getViewParameters(self):
        result, eye, lookat, up = self._scene_viewer.getLookatParameters()
        if result == OK:
            angle = self._scene_viewer.getViewAngle()
            return eye, lookat, up, angle
        return None

    def setViewParameters(self, eye, lookat, up, angle):
        self._scene_viewer.beginChange()
        self._scene_viewer.setLookatParametersNonSkew(eye, lookat, up)
        self._scene_viewer.setViewAngle(angle)
        self._scene_viewer.endChange()

    def setScenefilter(self, scenefilter):
        self._scene_viewer.setScenefilter(scenefilter)
        self._interactiveTool.setScenefilter(scenefilter)

    def getScenefilter(self):
        result, scenefilter = self._scene_viewer.getScenefilter()
        if result == OK:
            return scenefilter
        return None

    def project(self, x, y, z):
        in_coords = [x, y, z]
        fieldmodule = self._global_coords_from.getFieldmodule()
        fieldcache = fieldmodule.createFieldcache()
        self._global_coords_from.assignReal(fieldcache, in_coords)
        result, out_coords = self._window_coords_to.evaluateReal(fieldcache, 3)
        if result == OK:
            return out_coords
        return None

    def unproject(self, x, y, z):
        in_coords = [x, y, z]
        fieldmodule = self._window_coords_from.getFieldmodule()
        fieldcache = fieldmodule.createFieldcache()
        self._window_coords_from.assignReal(fieldcache, in_coords)
        result, out_coords = self._global_coords_to.evaluateReal(fieldcache, 3)
        if result == OK:
            return out_coords
        return None

    def getViewportSize(self):
        result, width, height = self._scene_viewer.getViewportSize()
        if result == OK:
            return width, height
        return None

    def setTumbleRate(self, rate):
        self._scene_viewer.setTumbleRate(rate)

    def viewAll(self):
        """
        Helper method to set the current scene viewer to view everything
        visible in the current scene.
        """
        self._scene_viewer.viewAll()

    def paintGL(self):
        """
        Render the scene for this scene viewer.  The QGLWidget has already set up the
        correct OpenGL buffer for us so all we need do is render into it.  The scene viewer
        will clear the background so any OpenGL drawing of your own needs to go after this
        API call.
        """
        self._scene_viewer.renderScene()

    def setupNodeGroups(self, distance):
        """
        This function creates the additional fields and Graphics for the Creatation of the nodes.
        It assumes the finite element field is already created and then creates the nodegroup field
        called Created_Nodes and graphics to display it.

        Created Nodes makes sure that the current nodes are the ones with the same z coordinate and
        that are in the node_cloud group.
        :return:
        """

        with get_material_module(self._context) as material_module:
            gold_material = material_module.findMaterialByName('gold')

        # Create the node graphics which are only needed if there are some images loaded in.
        region = self._region
        with get_field_module(region) as field_module:
            finite_element_field = field_module.findFieldByName('coordinates')
            image_number = field_module.findFieldByName('Image_Number')
            node_group = field_module.findFieldByName('node_cloud.nodes').castNodeGroup()

            distance_field = field_module.createFieldConstant(distance)
            image_pos = field_module.createFieldMultiply(distance_field, image_number)
            z = field_module.createFieldComponent(finite_element_field, 3)
            tolerance_field = field_module.createFieldConstant(0.1)
            zero = field_module.createFieldSubtract(image_pos, z)
            abs_zero = field_module.createFieldAbs(zero)
            and_field = field_module.createFieldLessThan(abs_zero, tolerance_field)
            current_nodes = field_module.createFieldAnd(and_field, node_group)
            current_nodes.setManaged(True)

            with get_scene(region) as scene:
                points = scene.createGraphicsPoints()
                points.setName("Created_Nodes")
                points.setCoordinateField(finite_element_field)
                points.setMaterial(gold_material)
                points.setFieldDomainType(Field.DOMAIN_TYPE_NODES)
                points.setSubgroupField(current_nodes)

                attributes = points.getGraphicspointattributes()
                attributes.setBaseSize(8)
                attributes.setGlyphShapeType(Glyph.SHAPE_TYPE_SPHERE)
                attributes.setLabelOffset([0.2, 0.2, 0.2])
                # attributes.setLabelField(node_number_field)

    @QtCore.Slot(int)
    def displayImageandNodes(self, image):
        """
        images: A single value corresponding to the image number that should be displayed along with it's nodes
        """

        self.viewImage(image)

        if self._interactiveTool is not None:
            size = self._region.getFieldmodule().findNodesetByName('nodes').getSize()
            if size > 0:
                self._interactiveTool.clearSelection()

    def mousePressEvent(self, event):
        """
        Inform the scene viewer of a mouse press event.
        """
        event.accept()
        self._interactiveTool.proceedSceneViewerMousePressEvent(event)

    def mouseReleaseEvent(self, event):
        """
        Inform the scene viewer of a mouse release event.
        """
        event.accept()
        self._interactiveTool.proceedSceneViewerMouseReleaseEvent(event)
        # if self._volumeTool is not None:
        #     self.update_volume.emit(self._volumeTool.calculateVolume())

    def mouseMoveEvent(self, event):
        """
        Inform the scene viewer of a mouse move event and update the OpenGL scene to reflect this
        change to the viewport.
        """
        event.accept()
        self._interactiveTool.proceedSceneViewerMouseMoveEvent(event)

    def wheelEvent(self, event):
        """
        Controls what happens when the mouse button is removed.
        :return:
        """

        numDegrees = event.delta() / 8
        numSteps = numDegrees / 15

        if event.orientation() == QtCore.Qt.Horizontal:
            event.ignore()
        else:
            self.update_slider.emit(numSteps)
            event.accept()

    def resizeGL(self, width, height):
        """
        Respond to widget resize events.
        """
        self._scene_viewer.setViewportSize(width, height)

    def _zincSceneviewerEvent(self, event):
        """
        Process a scene viewer event.  The updateGL() method is called for a
        repaint required event all other events are ignored.
        """
        if event.getChangeFlags() & Sceneviewerevent.CHANGE_FLAG_REPAINT_REQUIRED:
            QtCore.QTimer.singleShot(0, self.updateGL)

    def viewImage(self, number):
        """
        Changes the currently displayed image to number
        :param number: Integer that is the current image to be displayed.
        :return: True on success else False
        """
        self._current_image = number
        region = self._region
        if region.isValid():
            with get_field_module(region) as field_module:
                plane_coordinates = field_module.findFieldByName('Image_Number')
                if plane_coordinates.isValid():
                    field_cache = field_module.createFieldcache()
                    if plane_coordinates.assignReal(field_cache, number):
                        return True
        return False

    def toggleImages(self, state):
        """
        Makes the images appear or disappear
        :param state: True makes them visible and False hids them.
        :return: True on Success else False
        """

        region = self._region
        if region.isValid():
            with get_scene(region) as scene:
                image_pane = scene.findGraphicsByName('Image_Plane')
                if image_pane.getVisibilityFlag() != state:
                    image_pane.setVisibilityFlag(state)
                    self.toggleIsolines(state)
            return True
        return False

    def toggleIsolines(self, state):
        """
        Makes the iso-lines appear or disappear
        :param state: True makes them visible and False hids them.
        :return: True on Success else False
        """

        region = self._region
        if region.isValid():
            with get_scene(region) as scene:
                iso_lines = scene.findGraphicsByName('IsoLines')
                if iso_lines.getVisibilityFlag() != state:
                    iso_lines.setVisibilityFlag(state)
                return True
        return False

    def saveNodesToFile(self, filename):
        """
        Writes the node_cloud and coordinates to a filename.exnode in the EX format.
        :param filename:
        :return:
        """
        region = self._region

        region.beginChange()
        stream = region.createStreaminformationRegion()
        resource = stream.createStreamresourceFile(str(filename))
        stream.setResourceDomainTypes(resource, Field.DOMAIN_TYPE_NODES)
        stream.setResourceGroupName(resource, 'node_cloud')
        stream.setResourceFieldNames(resource, ['coordinates'])

        if region.write(stream):
            region.endChange()
            return True
        region.endChange()
        return False

    def viewNodes(self, state=None):
        """
        Toggles the Visibility of the Nodes.
        :return: None
        """

        region = self._region

        with get_scene(region) as scene:
            Graphics_Surface = scene.findGraphicsByName('Created_Nodes')
            if Graphics_Surface.isValid() and Graphics_Surface.getVisibilityFlag != state:
                Graphics_Surface.setVisibilityFlag(state)
            Graphics_Surface = scene.findGraphicsByName('Node_Cloud')
            if Graphics_Surface.isValid() and Graphics_Surface.getVisibilityFlag != state:
                Graphics_Surface.setVisibilityFlag(state)

    def viewMesh(self, state=None):
        """
        This sets the visibility of the internal 3D Liver Mesh. It with either be Solid, Transparent or None
        :param state: The string that will set the type
        :return:
        """

        if state is not None:
            if state == 'Transparent':
                self.setFlag('mesh', True, 'trans')
                self.setFlag('mesh', False, 'solid')
            elif state == 'Solid':
                self.setFlag('mesh', False, 'trans')
                self.setFlag('mesh', True, 'solid')
            elif state == 'None':
                self.setFlag('mesh', False, 'trans')
                self.setFlag('mesh', False, 'solid')

        region = self._region
        with get_scene(region) as scene:
            Graphic = scene.findGraphicsByName('Mesh_Elements')
            if self._flag_mesh_solid:
                Graphic.setVisibilityFlag(True)
                material = Graphic.getMaterial()
                material.setAttributeReal(Material.ATTRIBUTE_ALPHA, 1.0)
            elif self._flag_mesh_trans:
                Graphic.setVisibilityFlag(True)
                material = Graphic.getMaterial()
                material.setAttributeReal(Material.ATTRIBUTE_ALPHA, 0.5)
            else:
                Graphic.setVisibilityFlag(False)

    def toggleVisibility(self, enabled, graphic_name=None):
        region = self._region

        with get_scene(region) as scene:
            if graphic_name is None:
                scene.setVisibilityFlag(enabled)
            else:
                graphic = scene.findGraphicsByName(graphic_name)
                graphic.setVisibilityFlag(enabled)

    def undo(self):
        self._interactiveTool.undoLastAction()

    def redo(self):
        self._interactiveTool.redoLastAction()

    def findExtremePoints(self, nodeset='nodes', coordinates='coordinates'):
        """
        Finds the min and max x,y & z coordinates. It is achieved by looping through all the loads that are loaded
        in and storing the maximums and minimums.
        :param nodeset: The nodeset to find the extreme coordinates.
        :param coordinates: The coordinates field.
        :return: Returns a list of coordinates else None
        """

        region = self._region
        with get_field_module(region) as field_module:
            nodeset = field_module.findNodesetByName(nodeset)
            coordinates = field_module.findFieldByName(coordinates)
            fieldcache = field_module.createFieldcache()

            node_max = field_module.createFieldNodesetMaximum(coordinates, nodeset)
            node_min = field_module.createFieldNodesetMinimum(coordinates, nodeset)

            max = node_max.evaluateReal(fieldcache, 3)
            min = node_min.evaluateReal(fieldcache, 3)

            answers = max[1] + min[1]

        return answers

    def createFixedMesh(self, extremePoints, layers=5, values=20, radius=150, sin=math.sin, cos=math.cos):
        """
        This function creates the "cylinder capped mesh based off the two extreme points.
        It first finds the vector that defines the center axis. Using this and the first point we discover the vector
        that is perpendicular to the axis with an x = 1 as we need to fix a value. Then using the two vectors to
        calculate a third vector orthogonal to both to parametrise a circle based off t. This is then used to generate
        the number the layers and points per layer. The layer is then incremented by the distance vector.
        Then the elements are then created.
        :param extremePoints: The two points to generate the mesh between. The mesh will be a rectangular prism with the
        :param values: The number of nodes per layers.
        :param layers: The number of "layers" to generate
        :param radius: The radius of the sphere. Later the nodes will be placed on the
        :return: None
        """

        trans_distance = np.array(extremePoints[:3])
        distance_vector = [extremePoints[i+3] - extremePoints[i] for i in range(3)]
        distance = np.linalg.norm(distance_vector)

        vector_a = np.array([0, 0, 1])
        vector_b = distance_vector / np.linalg.norm(distance_vector)

        circle = [np.array([0, 0, -distance/layers])]
        rads = [2 * math.pi * i / values for i in range(values)]

        for i in range(layers + 1):
            for theta in rads:
                circle.append(
                    np.array(
                        [radius * sin(theta),
                         radius * cos(theta),
                         i * distance / layers],
                        dtype=np.float64)
                )
        circle.append(np.array([0, 0, distance + distance/layers]))

        cross = np.cross(vector_a, vector_b)
        sin_angle = np.linalg.norm(cross)
        cos_angle = np.dot(vector_a, vector_b)
        v = np.array([
            [0, -cross[2], cross[1]],
            [cross[2], 0, -cross[0]],
            [-cross[1], cross[0], 0]
        ], dtype=np.float64)

        R = np.eye(3) + v + np.linalg.matrix_power(v, 2) * (1 - cos_angle) / pow(sin_angle, 2)

        # Apply the rotation matrix to the cylinder!
        for i, coord in enumerate(circle):
            circle[i] = np.array(np.dot(R, coord) + trans_distance).tolist()

        # Create nodes and an element to see the progress.

        region = self._context.getDefaultRegion()

        with get_field_module(region) as field_module:
            field_name = 'mesh_coordinates'
            finite_element_field = field_module.findFieldByName(field_name)

            nodeset = field_module.findNodesetByName('nodes')
            field_node_group = field_module.createFieldNodeGroup(nodeset)
            field_node_group.setName('Mesh_Nodes')
            field_node_group.setManaged(True)
            nodeset_group = field_node_group.getNodesetGroup()
            element_group = field_module.createFieldElementGroup(field_module.findMeshByDimension(2))
            element_group.setName('Mesh_Elements')
            element_group.setManaged(True)

            element_group = field_module.findMeshByName('Mesh_Elements').castGroup()
            if not element_group.isValid():
                return False

            node_template = nodeset.createNodetemplate()
            node_template.defineField(finite_element_field)
            field_cache = field_module.createFieldcache()

            node_identifiers = []

            # Create the "tip" nodes
            for coord in circle:
                node = nodeset.createNode(-1, node_template)
                node_identifiers.append(node.getIdentifier())
                field_cache.setNode(node)
                finite_element_field.assignReal(field_cache, coord)
                nodeset_group.addNode(node)

            tip_identifiers = [node_identifiers.pop(0), node_identifiers.pop()]

            mesh = field_module.findMeshByDimension(2)
            element_template = mesh.createElementtemplate()
            element_template.setElementShapeType(Element.SHAPE_TYPE_SQUARE)
            element_node_count = 4
            element_template.setNumberOfNodes(element_node_count)
            linear_basis = field_module.createElementbasis(2, Elementbasis.FUNCTION_TYPE_LINEAR_LAGRANGE)
            node_indexes = [1, 2, 3, 4]
            element_template.defineFieldSimpleNodal(finite_element_field, -1, linear_basis, node_indexes)

            for i in range(layers):
                for j in range(values - 1):  # Exception for the final element that isn't standard
                    for n in range(2):
                        node = nodeset.findNodeByIdentifier(node_identifiers[j + values * (i - 1) + n])
                        if not node.isValid():
                            print("node is not valid")
                        element_template.setNode(n + 1, node)
                        node = nodeset.findNodeByIdentifier(node_identifiers[j + values * i + n])
                        if not node.isValid():
                            print("node is not valid")
                        element_template.setNode(n + 3, node)
                    element = mesh.createElement(-1, element_template)
                    if not element.isValid():
                        print("Error defining Element in Mesh")
                    element_group.addElement(element)

                node = nodeset.findNodeByIdentifier(node_identifiers[values + values * (i - 1) - 1])
                element_template.setNode(1, node)
                node = nodeset.findNodeByIdentifier(node_identifiers[values + values * i - 1])
                element_template.setNode(2, node)
                node = nodeset.findNodeByIdentifier(node_identifiers[values * (i - 1)])
                element_template.setNode(3, node)
                node = nodeset.findNodeByIdentifier(node_identifiers[values * i])
                element_template.setNode(4, node)
                element = mesh.createElement(-1, element_template)
                if not element.isValid():
                    print("Error defining Element in the mesh")
                    return False
                element_group.addElement(element)

            # Generate the Triangular Elements

            element_tip_template = mesh.createElementtemplate()
            element_tip_template.setElementShapeType(Element.SHAPE_TYPE_TRIANGLE)
            element_tip_template.setNumberOfNodes(3)
            linear_basis = field_module.createElementbasis(2, Elementbasis.FUNCTION_TYPE_LINEAR_SIMPLEX)
            node_indexes = [1, 2, 3]
            element_tip_template.defineFieldSimpleNodal(finite_element_field, -1, linear_basis, node_indexes)

            # The nodes are already created. Bottom/First
            node = nodeset.findNodeByIdentifier(tip_identifiers[0])
            element_tip_template.setNode(3, node)

            # Testing
            nodes = itertools.cycle(node_identifiers[:values])
            node = nodeset.findNodeByIdentifier(nodes.next())
            element_tip_template.setNode(1, node)
            for n in range(20):
                node = nodeset.findNodeByIdentifier(nodes.next())
                element_tip_template.setNode((n + 1) % 2 + 1, node)
                element = mesh.createElement(-1, element_tip_template)
                if element.isValid():
                    element_group.addElement(element)
                    continue
                else:
                    print("Error defining element")

            node = nodeset.findNodeByIdentifier(tip_identifiers[1])
            element_tip_template.setNode(3, node)

            nodes = itertools.cycle(node_identifiers[len(node_identifiers) - values:])
            node = nodeset.findNodeByIdentifier(nodes.next())
            element_tip_template.setNode(1, node)
            for n in range(20):
                node = nodeset.findNodeByIdentifier(nodes.next())
                element_tip_template.setNode((n + 1) % 2 + 1, node)
                element = mesh.createElement(-1, element_tip_template)
                if element.isValid():
                    element_group.addElement(element)
                    continue
                else:
                    print("Error defining element")

        return True

    def createGenericMesh(self, extremePoints, nodeNum=None):
        """
        Creates a generic mesh from 6 extreme points. The mesh is created with 2D Elements.
        First is generates the absolute X, Y and Z distances.
        If the node number is set then it divides up the three nodes equally between the three distances.
        Else it finds the number of nodes that are in the Created nodes, nodegroup and uses that.
        Then it generates the cylinder nodes to connect the 8 extreme points. The extreme points are given 5% increase
        Finally it creates adjacent 2D elements.
        :param extremePoints: A list in the form of X1, Y1, Z1, X2, Y2, Z2
        :return: None
        """

        region = self._region

        with get_field_module(region) as field_module:

            finite_element_field = field_module.findFieldByName('coordinates')

            node_group = field_module.createFieldNodeGroup(field_module.findNodesetByName('nodes'))
            node_group.setName('Mesh_Nodes')
            node_group.setManaged(True)
            element_group = field_module.createFieldElementGroup(field_module.findMeshByDimension(2))
            element_group.setName('Mesh_Elements')
            element_group.setManaged(True)

            distance = [abs(extremePoints[i] - extremePoints[i+3]) for i in range(3)]

            if nodeNum is None:
                # TODO make it count the number of nodes in the nodeset
                nodeNum = 27

            extremePoints = [extremePoints[i] * 1.15 for i in range(len(extremePoints))]

            for i, extremePoint in enumerate(extremePoints):

                if extremePoint > 0:
                    extremePoints[i] += distance[i % 3] * 0.15
                else:
                    extremePoints[i] -= distance[i % 3] * 0.15

            minExtreme = extremePoints[3:]
            maxExtreme = extremePoints[:3]

            Result = [minExtreme]
            Faces = []
            A = range(1, 6, 2)
            B = range(2, 7, 2)

            for i in range(3):

                value = list(minExtreme)
                value[i] = maxExtreme[i]
                Result.append(value)

                value = list(maxExtreme)
                value[i] = minExtreme[i]
                Result.append(value)

                value = [0] + A[:i] + A[i+1:] + [B[i]]
                Faces.append(value)
                value = [7] + B[:i] + B[i+1:] + [A[i]]
                Faces.append(value)

            Result.append(maxExtreme)
            num_sections = int(math.ceil(math.pow(nodeNum, 1.0/3.0)))
            for face in Faces:
                node_coordinates = [Result[i] for i in face]
                self.createMeshElements(node_coordinates, num_sections)

        with get_material_module(self._context) as material_module:
            trans_tissue = material_module.findMaterialByName('tissue')
            trans_tissue.setAttributeReal(Material.ATTRIBUTE_ALPHA, 0.3)

        with get_field_module(region) as field_module:
            finite_element_field = field_module.findFieldByName('mesh_coordinates')

            # with get_scene(region) as scene:
            #     surface = scene.createGraphicsSurfaces()
            #     surface.setSubgroupField(element_group)
            #     surface.setCoordinateField(finite_element_field)
            #     surface.setMaterial(trans_tissue)
            #     surface.setName('Generated_Mesh')

    def fitMesh(self):
        """
        This function fits the mesh to the surface.
        First creates the objective Field.
        :return: None
        """
        region = self._region
        with get_field_module(region) as field_module:

            finite_element_field = field_module.findFieldByName('mesh_coordinates')
            stored_location = field_module.findFieldByName('Stored_Locations')
            data_coordinates = field_module.findFieldByName('coordinates')
            data_nodeset = field_module.findNodesetByName('node_cloud_nodes')

            projected_coordinates = field_module.createFieldEmbedded(finite_element_field, stored_location)
            error_vector = field_module.createFieldSubtract(projected_coordinates, data_coordinates)
            outside_fit = field_module.createFieldNodesetSumSquares(error_vector, data_nodeset)

            # Minimise surface Area
            cache = field_module.createFieldcache()
            mesh = field_module.findMeshByDimension(2)
            constant = field_module.createFieldConstant(1.0)
            surface_area = field_module.createFieldMeshIntegral(constant, finite_element_field, mesh)
            surface_area.setNumbersOfPoints([3, 3, 3])
            result, volume = surface_area.evaluateReal(cache, 1)
            if result == OK:
                print(volume)

            weight = field_module.createFieldConstant(0.3)
            # Weighted Volume field:
            weighted_volume = field_module.createFieldMultiply(surface_area, weight)

            optimisation = field_module.createOptimisation()
            # optimisation.setMethod(Optimisation.METHOD_LEAST_SQUARES_QUASI_NEWTON)
            optimisation.setAttributeReal(Optimisation.ATTRIBUTE_FUNCTION_TOLERANCE, 1e-6)
            optimisation.setAttributeReal(Optimisation.ATTRIBUTE_GRADIENT_TOLERANCE, 1e-6)
            optimisation.setAttributeReal(Optimisation.ATTRIBUTE_LINESEARCH_TOLERANCE, 1e-5)
            optimisation.setAttributeReal(Optimisation.ATTRIBUTE_MAXIMUM_STEP, 10)
            optimisation.setAttributeInteger(Optimisation.ATTRIBUTE_MAXIMUM_ITERATIONS, int(1e2))
            optimisation.setAttributeInteger(Optimisation.ATTRIBUTE_MAXIMUM_FUNCTION_EVALUATIONS, int(1e4))
            optimisation.addObjectiveField(outside_fit)
            # optimisation.addObjectiveField(surface_area)
            optimisation.addObjectiveField(weighted_volume)
            optimisation.addIndependentField(finite_element_field)

            result = optimisation.optimise()
            if result != OK:
                print("Error!")
            print(optimisation.getSolutionReport())

            # self.updateGL()

    def createMeshElements(self, node_coordinates, num_sections=1):
        """
        This function generates a number of elements on a face between 4 points with a certain number of faces.
        :param node_coordinates:
        :param num_sections:
        :return:
        """

        region = self._region

        with get_field_module(region) as field_module:
            nodeset = field_module.findNodesetByName('mesh_nodes').castGroup()
            data_coordinates = field_module.findFieldByName('coordinates')
            finite_element_field = field_module.findFieldByName('mesh_coordinates')
            if not finite_element_field.isValid():
                finite_element_field = field_module.createFieldFiniteElement(3)
                finite_element_field.setName('mesh_coordinates')
                finite_element_field.setManaged(True)
                finite_element_field.setTypeCoordinate(True)

            node_template = nodeset.createNodetemplate()
            node_template.defineField(finite_element_field)
            field_cache = field_module.createFieldcache()

            mesh = field_module.findMeshByName('mesh_elements')

            stored_location = field_module.findFieldByName('Stored_Locations')
            found_location = field_module.createFieldFindMeshLocation(data_coordinates, finite_element_field, mesh)
            found_location.setSearchMode(FieldFindMeshLocation.SEARCH_MODE_NEAREST)

            if not stored_location.isValid():
                stored_location = field_module.createFieldStoredMeshLocation(mesh)
                stored_location.setManaged(True)
                stored_location.setName('Stored_Locations')

            mesh = mesh.castGroup()

            node_identifiers = []
            index = []
            coordinates = []

            for i, node_coordinate in enumerate(zip(*node_coordinates)):
                if len(set(node_coordinate)) == 1:
                    index = (i, node_coordinate[0])

            for i, node_coordinate in enumerate(zip(*node_coordinates)):
                if i == index[0]:
                    continue
                coordinates.append(sorted(list(set(node_coordinate))))

            ran = [i * 1.0 / num_sections for i in range(num_sections + 1)]
            istart = coordinates[0][0]
            jstart = coordinates[1][0]
            idist = coordinates[0][1] - istart
            jdist = coordinates[1][1] - jstart
            node_coordinates = []

            # Creates the nodes and adds to the node_coordinates list
            for i in ran:
                for j in ran:
                    value = [i * idist + istart, j * jdist + jstart]
                    value.insert(*index)
                    node_coordinates.append(value)
                    node = nodeset.createNode(-1, node_template)
                    node_identifiers.append(node.getIdentifier())
                    field_cache.setNode(node)
                    finite_element_field.assignReal(field_cache, value)

            element_template = mesh.createElementtemplate()
            element_template.setElementShapeType(Element.SHAPE_TYPE_SQUARE)
            element_node_count = 4
            element_template.setNumberOfNodes(element_node_count)
            # Specify the dimension and the interpolation function for the element basis function.
            linear_basis = field_module.createElementbasis(2, Elementbasis.FUNCTION_TYPE_LINEAR_LAGRANGE)
            # The indexes of the nodes in the node template we want to use
            node_indexes = [1, 2, 3, 4]
            # Define a nodally interpolated element field or field component in the
            # element_template. Only Lagrange, simplex and constant basis function types
            # may be used with this function, i.e. where only a simple node value is
            # mapped. Shape must be set before calling this function.  The -1 for the component number
            # defines all components with identical basis and nodal mappings.
            element_template.defineFieldSimpleNodal(finite_element_field, -1, linear_basis, node_indexes)

            node_list = [0, 1, num_sections + 1, num_sections + 2]

            for i in range(num_sections ** 2):
                for j, n in enumerate(node_list):
                    node = nodeset.findNodeByIdentifier(node_identifiers[n])
                    element_template.setNode(j+1, node)

                mesh.defineElement(-1, element_template)

                if i % num_sections == 2:
                    node_list = [i + 2 for i in node_list]
                else:
                    node_list = [i + 1 for i in node_list]

            nodeset = field_module.findNodesetByName('node_cloud_nodes')
            node_template = nodeset.createNodetemplate()
            node_template.defineField(stored_location)
            # Set up the StoredField
            node_iter = nodeset.createNodeiterator()
            node = node_iter.next()
            while node.isValid():
                field_cache.setNode(node)
                element, xi = found_location.evaluateMeshLocation(field_cache, 3)
                node.merge(node_template)
                if element.isValid():
                    stored_location.assignMeshLocation(field_cache, element, xi)
                node = node_iter.next()

    def createNodes(self, distance):
        """
        This function is called when the Create Nodes button is called. There are various cases.
        Create -> Create: No need to reload anything.
        Create -> Load: Remove the nodesets and graphics and reload
        Load -> Create: Remove the nodesets, graphics and reload

        Assumes that the node_cloud_nodes is either empty or no created.

        :return:
        """

        node_region = self._region

        with get_field_module(node_region) as field_module:
            image_number = field_module.findFieldByName('Current_Nodes')
            node_subgroup = field_module.findNodesetByName('node_cloud')

        if image_number.isValid():
            # Created graphics already loaded.
            # Do nothing and just continue
            pass
        elif node_subgroup.isValid() and node_subgroup.getSize() > 0:
            # Loaded graphics beforehand
            if self.unloadNodes():
                return True
        else:
            # First Load
            self.setupNodeGroups(distance)
            if self.createNodeGraphics():
                return True
        return False

    def loadNodes(self, filename, group=None):
        """
        Remove the current nodes in the Zinc Context. Removes the node groups.
        :param filename: The name of the file to be loaded into the 'nodes' region
        :param group: The name of the group that should be loaded from the file
        :return: True if successful else False
        """

        field_module = self._region.getFieldmodule()
        nodegroup = field_module.findNodesetByName('node_cloud.nodes')

        if nodegroup.isValid() and nodegroup.getSize() > 0:
            field_module.beginChange()
            nodegroup.destroyAllNodes()
            field_module.endChange()
            self.loadNodeData(filename, group)
        else:
            self.loadNodeData(filename, group)
            self.createNodeGraphics()

    def createNodeGraphics(self):
        """
        This function creates the node graphics that are used to display the nodes used to create.
        :return:
        """
        region = self._region

        with get_material_module(region) as material_module:
            green_material = material_module.findMaterialByName('green')
            blue_material = material_module.findMaterialByName('blue')

        with get_field_module(region) as field_module:
            finite_element_field = field_module.findFieldByName('coordinates')
            nodesetGroup = field_module.findFieldByName('node_cloud')

        if finite_element_field.isValid() and nodesetGroup.isValid():
            with get_scene(region) as scene:
                points = scene.createGraphicsPoints()
                points.setCoordinateField(finite_element_field)
                points.setMaterial(blue_material)
                points.setSelectedMaterial(green_material)
                points.setFieldDomainType(Field.DOMAIN_TYPE_NODES)
                points.setName('Node_Cloud')
                points.setSubgroupField(nodesetGroup)

                attributes = points.getGraphicspointattributes()
                attributes.setGlyphShapeType(Glyph.SHAPE_TYPE_SPHERE)
                attributes.setBaseSize(5)
                return True
        return False

    def unloadNodes(self):
        """
        Removes all the nodes/elements in the node_cloud nodesetGroup().
        :return:
        """

        region = self._region
        with get_field_module(region) as field_module:
            nc_group = field_module.findFieldByName('node_cloud')
            if nc_group.isValid():
                result = nc_group.clearLocal()
            else:
                return False
        if result == OK:
            return True

    def loadNodeData(self, filename, group='node_cloud'):
        """
        Loads in the Node Data from filenames.
        :param filename:
        :param group:
        :return:
        """
        region = self._region

        region.beginChange()
        StreamInformation = region.createStreaminformationRegion()
        Stream_file = StreamInformation.createStreamresourceFile(filename)
        StreamInformation.setFieldNames(["coordinates"])
        StreamInformation.setResourceGroupName(Stream_file, group)
        region.read(StreamInformation)
        region.endChange()

        with get_field_module(region) as field_module:
            coordinates = field_module.findFieldByName('coordinates')
            coordinates.setTypeCoordinate(True)

    def loadMesh(self, filenames, group='mesh'):
        """
        Logic for checking if certain groups, fields and regions are valid and exist.
        :param filenames: The file(s) that contain the nodes and elements of the Mesh.
        :param group: Optional parameter that specifies what Group should be loaded in
        :return: True if successful else False
        """

        region = self._region

        with get_field_module(region) as field_module:
            mesh = field_module.findMeshByName('mesh.mesh3d')

        if mesh.isValid() and mesh.getSize() > 0:
            if self.unloadMesh() and self.loadMeshData(filenames, group):
                self.createBoundingBox()
                return True
        else:
            if self.loadMeshData(filenames, group) and self.createMeshGraphics():
                self.createBoundingBox()
                self.createBoundingBoxGraphics()
                return True
        return False

    def generateMesh(self):
        """
        :return:
        """
        nodes = self.findExtremePoints('Node_Cloud.nodes')
        if nodes is not None:
            if self.createFixedMesh(nodes):
                self.createMeshGraphics()
                # self.fitMesh()
                self.createBoundingBox()
                self.createBoundingBoxGraphics()

    def unloadMesh(self):
        """
        Removes the loaded Elements, Nodes. The graphics are not moved at all. The subgroups remain intact.
        :return:
        """

        region = self._region

        # Remove the Mesh
        with get_field_module(region) as field_module:
            mesh_group = field_module.findFieldByName('mesh').castGroup()
            bounding_box_group = field_module.findFieldByName('bounding_box').castGroup()
            result_1 = mesh_group.clearLocal()
            result_2 = bounding_box_group.clearLocal()

            # Remove the fields for managing the Volume calculation
            volume_field = field_module.findFieldByName('Volume_Total')
            if volume_field.isValid():
                volume_field.setManaged(False)
            for i in range(8):
                volume_field = field_module.findFieldByName('Volume_{}'.format(i + 1))
                if volume_field.isValid():
                    volume_field.setManaged(False)

        if result_1 == OK and result_2 == OK:
            return True
        return False

    def loadMeshData(self, filenames, group='mesh'):
        """
        Loads in the two Mesh files that will create the graphics surface.
        :param filenames: The files to be loaded in the Mesh
        :param group: The name of the group that needs to be loaded in
        :return: True if successful else False
        """

        region = self._region
        region.beginChange()
        StreamInformation = region.createStreaminformationRegion()
        for filename in filenames:
            Stream_resource = StreamInformation.createStreamresourceFile(str(filename))
            StreamInformation.setFieldNames(["mesh_coordinates"])
            StreamInformation.setResourceGroupName(Stream_resource, 'mesh')
        result = region.read(StreamInformation)
        region.endChange()
        if result == OK:
            print("I am here")
            return True
        return False

    def createMeshGraphics(self):
        """
        Creates the Bounding Box graphics (Points & Lines). It also creates the Mesh (surface) graphics.
        :return:
        """

        region = self._region

        with get_field_module(region) as field_module:
            finite_element_field = field_module.findFieldByName('mesh_coordinates')
            mesh = field_module.findMeshByDimension(2)
            group = field_module.findFieldByName('mesh')

            if not group.isValid() or not finite_element_field.isValid():
                return False

            data_coordinates = field_module.findFieldByName('coordinates')
            stored_location = field_module.findFieldByName('Stored_Locations')

            image_field = field_module.findFieldByName('image_field_0')
            image_field.castImage().setDomainField(finite_element_field)

            if not stored_location.isValid():
                stored_location = field_module.createFieldStoredMeshLocation(mesh)
                stored_location.setManaged(True)
                stored_location.setName('Stored_Locations')

            if mesh.isValid() and data_coordinates.isValid():

                found_location = field_module.createFieldFindMeshLocation(data_coordinates, finite_element_field, mesh)
                found_location.setSearchMode(FieldFindMeshLocation.SEARCH_MODE_NEAREST)

                field_cache = field_module.createFieldcache()
                nodeset = field_module.findNodesetByName('mesh.nodes')
                node_template = nodeset.createNodetemplate()
                node_template.defineField(stored_location)

                node_iter = nodeset.createNodeiterator()
                node = node_iter.next()
                while node.isValid():
                    field_cache.setNode(node)
                    element, xi = found_location.evaluateMeshLocation(field_cache, 3)
                    node.merge(node_template)
                    if element.isValid():
                        stored_location.assignMeshLocation(field_cache, element, xi)
                    node = node_iter.next()

        with get_material_module(self._context) as material_module:
            trans_tissue = material_module.findMaterialByName('tissue')
            trans_tissue.setAttributeReal(Material.ATTRIBUTE_ALPHA, 0.6)
            trans_tissue.setManaged(True)

        with get_scene(region) as scene:
            surface = scene.createGraphicsSurfaces()
            surface.setCoordinateField(finite_element_field)
            surface.setMaterial(trans_tissue)
            surface.setName('Generated_Mesh')
            surface.setSubgroupField(group)
            surface.setExterior(True)

            iso_surface = scene.findGraphicsByName('IsoSurface')
            iso_surface.setCoordinateField(finite_element_field)
        return True

    def loadImages(self, filenames, image_info, imageset_number=0):
        """
        From a list of filenames, it creates the Image Textures, Graphics (isolines & surfaces), Container and
        NodeGroups.
        If there is currently Image Graphics or Element data they are removed. Then for each file there are elements,
        surfaces, texture and materials.
        :param filenames: The list of files to load in for the images
        :param image_info: Image object that
        :param imageset_number: The imageset number, which default to zero.
        :return: True if successful else False
        """

        # Check for unloading
        region = self._region

        # TODO check for unloading of Images
        # result = self.unloadImages()
        with get_field_module(region) as field_module:
            image_field = field_module.findFieldByName('image_field_{}'.format(imageset_number))
        if image_field.isValid():
            if self.reloadImages(filenames, image_info, imageset_number):
                return True
        else:
            self.createGroups()
            if self.loadImageData(filenames, image_info, imageset_number):
                print("Image data successfully loaded")
                num_images = image_info.getNumberOfImages()
                result = self.createImageGraphics(image_info, imageset_number)
                if result:
                    print("The Image graphics successfully created")
                    return True
        return False

    def createGroups(self):

        region = self._region
        names = ['pane', 'container', 'bounding_box']
        with get_field_module(region) as field_module:
            for name in names:
                group = field_module.createFieldGroup()
                group.setManaged(True)
                group.setName(name)

            node_group = field_module.createFieldGroup()
            node_group.setManaged(True)
            node_group.setName('node_cloud')
            nodeset = field_module.findNodesetByName('nodes')
            node_group.createFieldNodeGroup(nodeset)

            mesh_group = field_module.createFieldGroup()
            mesh_group.setManaged(True)
            mesh_group.setName('mesh')
            # mesh_group.createFieldNodeGroup(nodeset)

    def reloadImages(self, filenames, image_info, imageset_number=0):
        """
        Only removes the image fields as they need to be reloaded.

        Then loads in the new image field.
        :param : The imageset to be unloaded. An integer starting from zero.
        :return: True if successful else False
        """

        region = self._region
        size = image_info.getSize()
        count = image_info.getNumberOfImages()
        distance = image_info.getDistance()

        with get_field_module(region) as field_module:
            image_field = field_module.findFieldByName('image_field_{}'.format(imageset_number))
            image_field.setManaged(False)
            iso_surface = field_module.findFieldByName('Image_Field_Lines_{}'.format(imageset_number))
            iso_surface.setManaged(False)

            image_field = field_module.createFieldImage()
            image_field.setWrapMode(FieldImage.WRAP_MODE_CLAMP)
            image_field.setFilterMode(FieldImage.FILTER_MODE_LINEAR)
            # image_field.setManaged(True)
            # image_field.setName(name)
            image_field.setTextureCoordinateDepth(count * distance)
            image_field.setTextureCoordinateWidth(size[0])
            image_field.setTextureCoordinateHeight(size[1])

            image_field_lines = field_module.createFieldImageFromSource(image_field)

        stream_information = image_field.createStreaminformationImage()
        filenames.reverse()

        flag = stream_information.FILE_FORMAT_DICOM
        stream_information.setFileFormat(flag)

        for filename in filenames:
            stream_information.createStreamresourceFile(str(filename))
        image_field.read(stream_information)

        with get_material_module(region) as material_module:
            vein = material_module.findMaterialByName('vein')
            vein.setTextureField(1, image_field)

        with get_scene(region) as scene:
            iso = scene.findGraphicsByName('IsoSurface').castContours()
            iso.setIsoscalarField(image_field)

            iso_lines = scene.findGraphicsByName('IsoLines').castContours()
            iso_lines.setIsoscalarField(image_field_lines)

            surface = scene.findGraphicsByName('Image_Plane')
            surface.setMaterial(vein)

    def loadImageData(self, filenames, image_info, imageset_number=0):
        """
        For the current imageset image it loads in the images.
        Creates the Image Field with the correct texture depth.
        :param filenames:
        :param image_info:
        :param imageset_number:
        :return:
        """

        region = self._region
        size = image_info.getSize()
        count = image_info.getNumberOfImages()
        distance = image_info.getDistance()

        with get_field_module(region) as field_module:

            field_name = 'coordinates'
            finite_element_field = field_module.findFieldByName(field_name)
            if not finite_element_field.isValid():
                finite_element_field = field_module.createFieldFiniteElement(3)
                finite_element_field.setName(field_name)
                finite_element_field.setManaged(True)
                finite_element_field.setTypeCoordinate(True)

            name = 'image_field_{}'.format(imageset_number)
            image_field = field_module.createFieldImage()
            image_field.setWrapMode(FieldImage.WRAP_MODE_CLAMP)
            image_field.setFilterMode(FieldImage.FILTER_MODE_LINEAR)
            image_field.setManaged(True)
            image_field.setName(name)
            image_field.setTextureCoordinateDepth(count * distance)
            image_field.setTextureCoordinateWidth(size[0])
            image_field.setTextureCoordinateHeight(size[1])

            stream_information = image_field.createStreaminformationImage()
            filenames.reverse()

            flag = stream_information.FILE_FORMAT_DICOM
            stream_information.setFileFormat(flag)

            for filename in filenames:
                stream_information.createStreamresourceFile(str(filename))

        if image_field.read(stream_information) == OK:

            image_field_lines = field_module.createFieldImageFromSource(image_field)
            image_field_lines.setManaged(True)
            image_field_lines.setName('Image_Field_Lines_{}'.format(imageset_number))

            # Create the image plane
            node_coordinates = [[(i % 2) * size[0], (i // 2) * size[1], 0] for i in range(4)]
            zinc_static.create2DFiniteElement(field_module, finite_element_field, node_coordinates, group='pane')

            # Create the container
            box_size = [size[0], size[1], (count - 1) * distance]
            value = [1, 0]
            node_coordinates = [[i * box_size[0], j * box_size[1], k * box_size[2]] for i, j, k in itertools.product(value, value, value)]

            node_coordinates = itertools.combinations(node_coordinates, 2)
            for node_coordinate in node_coordinates:
                adjacent = map(lambda x, y: x - y == 0, node_coordinate[0], node_coordinate[1])
                if sum(adjacent) == 2:
                    zinc_static.create1DElement(field_module, finite_element_field, node_coordinate, group='container')

            with get_material_module(region) as material_module:
                vein = material_module.createMaterial()
                vein.setName('vein')
                vein.setManaged(True)
                vein.setTextureField(1, image_field)

            return True
        return False

    def createImageGraphics(self, image_info, imageset_number=0):
        """
        From the image field creates the isolines, isosurface, the image pane to display a certain image.
        :param imageset_number: The current image set.
        :return: True if successful else False
        """

        region = self._region

        with get_field_module(region) as field_module:
            finite_element_field = field_module.findFieldByName('coordinates')
            mesh_group = field_module.findFieldByName('mesh')

            image_number = field_module.createFieldConstant(0)
            image_number.setManaged(True)
            image_number.setName('Image_Number')

            distance = image_info.getDistance()
            distance_field = field_module.createFieldConstant(distance)
            image_pos = field_module.createFieldMultiply(image_number, distance_field)
            image_pos.setName('Image_Position')
            image_pos.setManaged(True)

            xy = field_module.createFieldComponent(finite_element_field, [1, 2])
            pane_coordinates = field_module.createFieldConcatenate([xy, image_pos])
            pane_coordinates.setName('pane_coordnates')
            pane_coordinates.setTypeCoordinate(True)
            pane_coordinates.setManaged(True)

            image_field = field_module.findFieldByName('image_field_{}'.format(imageset_number))
            # image_field.castImage().setDomainField(finite_element_field)

            name = 'Image_Field_Lines_{}'.format(imageset_number)
            image_field_lines = field_module.findFieldByName(name)
            image_field_lines.castImage().setDomainField(pane_coordinates)

            pane_group = field_module.findFieldByName('pane')
            container_group = field_module.findFieldByName('container')

        with get_material_module(region) as material_module:
            red_material = material_module.findMaterialByName('red')
            green_material = material_module.findMaterialByName('green')
            vein = material_module.findMaterialByName('vein')

        with get_scene(region) as scene:
            iso = scene.createGraphicsContours()
            iso.setFieldDomainType(Field.DOMAIN_TYPE_MESH3D)
            # iso.setCoordinateField(finite_element_field)
            iso.setListIsovalues([0.291])
            iso.setIsoscalarField(image_field)
            iso.setMaterial(green_material)
            iso.setName('IsoSurface')
            iso.setVisibilityFlag(False)
            iso.setSubgroupField(mesh_group)

            tes = iso.getTessellation()
            tes.setManaged(True)
            tes.setCircleDivisions(20)
            tes.setMinimumDivisions(100)
            tes.setRefinementFactors(4)
            iso.setTessellation(tes)

            iso_lines = scene.createGraphicsContours()
            iso_lines.setFieldDomainType(Field.DOMAIN_TYPE_MESH2D)
            iso_lines.setCoordinateField(pane_coordinates)
            iso_lines.setListIsovalues([0.291])
            iso_lines.setIsoscalarField(image_field_lines)
            iso_lines.setMaterial(red_material)
            iso_lines.setName('IsoLines')
            iso_lines.setVisibilityFlag(False)

            tessellation = iso_lines.getTessellation()
            tessellation.setMinimumDivisions([100, 100, 50])

            iso_lines.setTessellation(tessellation)

            # Create the Container
            lines = scene.createGraphicsLines()
            lines.setName('container')
            lines.setCoordinateField(finite_element_field)
            lines.setMaterial(red_material)
            lines.setSubgroupField(container_group)

            surface = scene.createGraphicsSurfaces()
            surface.setCoordinateField(pane_coordinates)
            surface.setTextureCoordinateField(pane_coordinates)
            surface.setMaterial(vein)
            surface.setName('Image_Plane')
            surface.setSubgroupField(pane_group)

        return True

    def createBoundingBox(self):
        """
        This function creates and displays the bounding box.
        Looks for the extreme values in the mesh and then increase the values by 10% and creates the
        Bounding Box.
        :return: True on success else False
        """

        extremePoints = self.findExtremePoints('mesh.nodes', 'mesh_coordinates')

        minExtreme = np.array(extremePoints[3:], dtype=np.float64)
        maxExtreme = np.array(extremePoints[:3], dtype=np.float64)
        distance = np.array(maxExtreme - minExtreme, dtype=np.float64)
        Result = []

        for combination in itertools.product([0, 0.5, 1], repeat=3):
            value = minExtreme + distance * combination
            Result.append(value.tolist())

        region = self._region
        with get_field_module(region) as field_module:
            finite_element_field = field_module.findFieldByName('mesh_coordinates')

        if finite_element_field.isValid():
            fieldCache = field_module.createFieldcache()

            nodeset = field_module.findNodesetByName('nodes')
            group = field_module.findFieldByName('bounding_box').castGroup()
            node_field_group = group.createFieldNodeGroup(nodeset)
            node_field_group = node_field_group.getNodesetGroup()

            mesh = field_module.findMeshByDimension(1)
            element_field_group = group.createFieldElementGroup(mesh)
            meshGroup = element_field_group.getMeshGroup()

            element_template = mesh.createElementtemplate()
            element_template.setElementShapeType(Element.SHAPE_TYPE_LINE)
            element_template.setNumberOfNodes(2)
            linear_basis = field_module.createElementbasis(1, Elementbasis.FUNCTION_TYPE_LINEAR_LAGRANGE)
            element_template.defineFieldSimpleNodal(finite_element_field, -1, linear_basis, [1, 2])
            keys = {
                0: (1, 3),
                2: (1, 5),
                6: (3, 7),
                8: (5, 7),
            }
            node_index = []

            if nodeset.isValid() and node_field_group.isValid():
                node_template = nodeset.createNodetemplate()
                node_template.defineField(finite_element_field)

                for point in Result:
                    node = nodeset.createNode(-1, node_template)
                    node_field_group.addNode(node)
                    fieldCache.setNode(node)
                    finite_element_field.assignReal(fieldCache, point)
                    node_index.append(node.getIdentifier())

                    # For each layer, create the values
                for n in range(3):
                    layer = node_index[9 * n:9*(n + 1)]
                    node = nodeset.findNodeByIdentifier(layer[4])
                    element_template.setNode(2, node)
                    for m in range(1, 9, 2):
                        node = nodeset.findNodeByIdentifier(layer[m])
                        element_template.setNode(1, node)
                        element = mesh.createElement(-1, element_template)
                        if element.isValid():
                            meshGroup.addElement(element)
                    for m in [0, 2, 6, 8]:
                        node = nodeset.findNodeByIdentifier(layer[m])
                        element_template.setNode(2, node)
                        for k in keys[m]:
                            node = nodeset.findNodeByIdentifier(layer[k])
                            element_template.setNode(1, node)
                            element = mesh.createElement(-1, element_template)
                            if element.isValid():
                                meshGroup.addElement(element)
                # layer = node_index[9:18]
                for n in range(9, 18):
                    node = nodeset.findNodeByIdentifier(node_index[n])
                    element_template.setNode(2, node)
                    node = nodeset.findNodeByIdentifier(node_index[n + 9])
                    element_template.setNode(1, node)
                    element = mesh.createElement(-1, element_template)
                    if element.isValid():
                        meshGroup.addElement(element)
                    node = nodeset.findNodeByIdentifier(node_index[n - 9])
                    element_template.setNode(1, node)
                    element = mesh.createElement(-1, element_template)
                    if element.isValid():
                        meshGroup.addElement(element)

        node_coordinates = []
        indexes = [0, 1, 3, 4, 9, 10, 12, 13]
        increment = [1, 2, 1, 5, 1, 2, 1]
        node_coordinates.append(indexes)

        for incre in increment:
            indexes = [i + increment[incre] for i in indexes]
            node_coordinates.append(indexes)

            if finite_element_field.isValid():

                nodeset = field_module.findNodesetByName('nodes')
                group = field_module.findFieldByName('bounding_box').castGroup()
                node_field_group = group.createFieldNodeGroup(nodeset)

                mesh = field_module.findMeshByDimension(3)
                element_field_group = group.createFieldElementGroup(mesh)

        with get_field_module(region) as field_module:

            for i, node_coordinate in enumerate(node_coordinates):
                node_coordinates[i] = [Result[j] for j in node_coordinate]

            for node_coordinate in node_coordinates:
                zinc_static.create3DFiniteElement(field_module, finite_element_field, node_coordinate, 'bounding_box')

    def createBoundingBoxGraphics(self):
        """

        :return:
        """

        node_region = self._region

        with get_field_module(node_region) as field_module:
            finite_element_field = field_module.findFieldByName('mesh_coordinates')
            BB_group = field_module.findFieldByName('bounding_box')
            if not BB_group.isValid or not finite_element_field.isValid():
                print("Error finding the bounding_box group")
                return False

        with get_material_module(node_region) as material_module:
            orange_material = material_module.findMaterialByName('orange')
            green_material = material_module.findMaterialByName('green')

        with get_scene(node_region) as scene:
            point = scene.createGraphicsPoints()
            point.setCoordinateField(finite_element_field)
            point.setName('BB_Points')
            point.setFieldDomainType(Field.DOMAIN_TYPE_NODES)
            point.setSubgroupField(BB_group)
            point.setMaterial(orange_material)

            attributes = point.getGraphicspointattributes()
            attributes.setBaseSize(15)
            attributes.setGlyphShapeType(Glyph.SHAPE_TYPE_SPHERE)

            lines = scene.createGraphicsLines()
            lines.setCoordinateField(finite_element_field)
            lines.setName('BB_Lines')
            lines.setMaterial(green_material)
            lines.setSubgroupField(BB_group)

        return True

    def setUpVolume(self, mesh='mesh', bbmesh='bounding_box', coordinate='mesh_coordinates'):

        if self._volumeTool is None:
            self._volumeTool = zinc_volume.zincVolume()

            with get_field_module(self._region) as field_module:

                finite_element_field = field_module.findFieldByName(coordinate)
                bb_mesh = field_module.findMeshByName('{}.mesh3d'.format(bbmesh))
                liver_mesh = field_module.findMeshByName('{}.mesh3d'.format(mesh))

            self._volumeTool.setRegion(self._region)
            self._volumeTool.setCoordinateField(finite_element_field)
            self._volumeTool.setMeshes(bb_mesh, liver_mesh)

        self._volumeTool.calculateVolume()

    def saveToFile(self, filename):

        stream_region = self._region.createStreaminformationRegion()
        node_file = stream_region.createStreamresourceFile('{}_nodes.exfile'.format(filename))
        mesh_file = stream_region.createStreamresourceFile('{}_mesh.exfile'.format(filename))
        bounding_box_file = stream_region.createStreamresourceFile('{}_bounding_box.exfile'.format(filename))
        stream_region.setResourceGroupName(node_file, 'node_cloud')
        stream_region.setResourceGroupName(mesh_file, 'mesh')
        stream_region.setResourceFieldNames(mesh_file, ['mesh_coordinates'])
        stream_region.setResourceGroupName(bounding_box_file, 'bounding_box')
        if self._region.write(stream_region):
            return True
        else:
            return False
