try:
    from PySide import QtGui, QtCore, QtGui
except ImportError:
    from PyQt4 import QtCore, QtGui

from zinc_helper import *
import zinc_interactive_static as zinc_static


class Undo(QtCore.QObject):

    undo_state = QtCore.Signal(int)
    redo_state = QtCore.Signal(int)

    def __init__(self, Object, size=5):

        super(Undo, self).__init__()

        self.limit = size
        self._previous = []
        self._redo_previous = []
        self._actions = {'delete node': 'removeNode', 'delete element': 'removeElement',
                         'create node': 'addNode', 'create element': 'addElement',
                         'move element': 'moveElement', 'move node': 'moveNode'}

        self._redo_actions = {'addNode': 'removeNode', 'addElement': 'removeElement',
                              'removeNode': 'addNode', 'removeElement': 'addElement',
                              'moveElement': 'moveElement', 'moveNode': 'moveNode'}

        self._object = Object

    def addAction(self, action, data=None):

        Callable = self._actions[action]
        self._previous.append((Callable, data))
        self._redo_previous = []
        self.undo_state.emit(True)
        self.redo_state.emit(False)

    def undo(self):
        """
        Signals for a method to be called on the object to undo the previous action.
        The redo list is then populated with the inverse reaction.
        Finally signals are emitted to update the UI.
        """
        Action, data = self._previous.pop()

        if Action == 'moveNode':
            data = ([-a for a in data[0]], data[1])

        getattr(self, Action)(data)

        # Add in the redo
        Callable = self._redo_actions[Action]
        self._redo_previous.append((Callable, data))

        # Signals to update the UI
        self.redo_state.emit(True)
        if len(self._previous) == 0:
            self.undo_state.emit(False)

    def redo(self):
        """
        Identical to the previous function except for different lists and signals being updated.
        """
        if len(self._redo_previous) == 0:
            raise IndexError

        Action, data = self._redo_previous.pop()

        if Action == 'moveNode':
            data = ([-a for a in data[0]], data[1])

        getattr(self, Action)(data)

        # Add back to redo
        Callable = self._redo_actions[Action]
        self._previous.append((Callable, data))

        self.undo_state.emit(True)
        if len(self._redo_previous) == 0:
            self.redo_state.emit(False)

    def addNode(self, nodegroup):
        """
        Inverse function to add in nodes that were just created.
        :param nodegroup: The Nodeset that is one at the nodes to be added.
        :return:
        """
        self._object.getCreateNodeGroup().addNodesConditional(nodegroup)

    def moveNode(self, data):
        """
        This inverse function moves a node to undo the distance selected nodes were moved.
        :param data: Tuple containing a tuple that contains the x, y and z change along with the nodes
        :return:
        """

        distance = data[0]
        nodes = data[1]

        iterator = self._object.getCreateNodeGroup().createNodeiterator()
        node = iterator.next()
        finite_element_field = self._object.getScenepicker().getNearestGraphics().getCoordinateField()

        with get_field_module(finite_element_field) as field_module:
            field_cache = field_module.createFieldcache()

            while node.isValid():
                if node.getIdentifier() in nodes:
                    zinc_static.updateNodePositionWithDelta(field_cache, finite_element_field,
                                                     node, distance[0], distance[1], distance[2])
                node = iterator.next()

    def removeNode(self, nodegroup):
        # Inputs: nodeset is the NodesetFieldGroup that will allow the removal of certain nodes.
        self._object.getCreateNodeGroup().removeNodesConditional(nodegroup)

    def addElement(self, meshField):
        # TODO implement
        mesh = meshField.getMeshGroup()
        self._selectionGroup.getFieldElementGroup(mesh).getMeshGroup().addElementsConitional(meshField)

    def removeElement(self, mesh):
        # TODO implement
        self._selectionGroup.getFieldElementGroup().getMeshGroup().removeElementsConitional()


