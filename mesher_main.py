"""
This program is a tool that will help to develop ways to make faster meshes than in CM GUI
Currently the meshing workflow in CM GUI is slow and has too many options.
The idea is to make a simpler version so the models from Adam can be made faster!

The project will involve some form of GUI of some description or do something completely different.
"""

from __future__ import print_function

try:
    from PySide import QtCore, QtGui
except ImportError:
    from PyQt4 import QtCore, QtGui

from opencmiss.zinc.context import Context
from zinc_helper import *

import os
import sys

from Image import Image
from wizard_ui import Ui_wizard
from node_creation_ui import Ui_MainWindow as MainUI


class ImportWizard(QtGui.QWizard):
    """
    This is the wizard for loading in the Images, Nodes and Mesh. There are options for creating nodes and generating a
    mesh.
    """

    def __init__(self):

        super(ImportWizard, self).__init__()

        self._ui = None
        self._context = None
        self._zinc = None
        self._volume = None

        self._valid_images = False
        self._valid_nodes = False
        self._valid_meshes = False

        self._node_file = None
        self._mesh_files = None
        self._image_files = None

        self.node_editing_dialouge = None

        self.setupUI()

    def setupUI(self):

        self._ui = Ui_wizard()
        ui = self._ui
        ui.setupUi(self)

        # Set up Zinc related commands
        self._context = Context("Fast Mesher")
        self._context.getGlyphmodule().defineStandardGlyphs()
        self._context.getMaterialmodule().defineStandardMaterials()
        self._zinc = self._ui.zincWidget
        self._zinc.setContext(self._context)

        # Assigning buttons #Images#
        self._ui.loadImages.clicked.connect(self.selectImages)

        ui.ImagePage.isComplete = self.isCompleteImage

        # Assigning buttons #Nodes#
        ui.NodePage.initializePage = self.loadImages
        ui.createNodes.clicked.connect(self.createNodes)
        ui.loadNodes.clicked.connect(self.loadNodes)
        ui.PreviewPage.initializePage = self.preparePreview

        # Assigning buttons #Meshes#
        ui.generateMesh.clicked.connect(self.generateMesh)
        ui.loadMesh.clicked.connect(self.loadMesh)

        ui.displayBoundingBox.toggled.connect(self.toggleBoundingBox)
        ui.displayContainer.toggled.connect(self.toggleContainer)
        ui.displayImages.toggled.connect(self.toggleImages)
        ui.displayIsoLines.toggled.connect(self.toggleIsoLines)
        ui.displayIsoSurface.toggled.connect(self.toggleIsoSurface)
        ui.displayMesh.toggled.connect(self.toggleMesh)
        ui.displayNodes.toggled.connect(self.toggleNodes)

        image_slider = self._ui.imageSlider
        image_slider.setValue(0)
        image_slider.setInvertedAppearance(True)

        ui.saveButton.clicked.connect(self.saveFinal)


    def saveFinal(self):

        save_name, extension = QtGui.QFileDialog.getSaveFileName(
            self,
            "Please enter project name and choose the directory",
            filter="EX File Format (*.exfile)"
        )

        if save_name and extension:
            if self._zinc.saveToFile(os.path.splitext(save_name)[0]):
                QtGui.QMessageBox.about(self, "Save status", "All files saved successfully")

    def preparePreview(self):

        self._zinc.toggleVisibility(True, 'Generated_Mesh')
        self._zinc.toggleVisibility(True, 'BB_Points')
        self._zinc.toggleVisibility(True, 'BB_Lines')
        self._zinc.toggleVisibility(False, 'IsoSurface')
        self._zinc.toggleVisibility(False, 'IsoLines')
        self._zinc.toggleImages(False)
        self._zinc.viewNodes(False)
        self._zinc.toggleVisibility(False, 'container')

        zincInteract = self._ui.zincWidget._interactiveTool

        zincInteract.setNodeCreateMode(False)
        zincInteract.setNodeSelection(True)
        zincInteract.setNodeEdit(True)
        zincInteract.setNodeDeleteMode(False)
        zincInteract.setSelectionModeAdditive(False)

        fm = self._zinc.getContext().getDefaultRegion().getFieldmodule()
        finite_element_field = fm.findFieldByName('coordinates')
        zincInteract.setNodeCreateCoordinatesField(finite_element_field)

        self._zinc.setUpVolume()
        zincInteract.update_volume.connect(self.updateVolume)

    def completeChangedImage(self):
        """
        A reimplementation of completeChanged for the Image Page. Ensures the Next Button Status is always checked.
        :return: None
        """
        self._ui.ImagePage.completeChanged.emit()

    def isCompleteImage(self):
        """
        Checks if the Image Page is valid and thus is able to continue.
        :return: True if the Form is filled out and there is valid inputs else False.
        """
        # return True

        if self._valid_images: #and self._ui.skipEveryXImageLineEdit.text():
            return True
        return False

    def loadImages(self):
        """
        Called before the node page is loaded to ensure the images are loaded.
        :return:
        """
        ui = self._ui

        # try:
        #     self._skipXfiles = int(ui.skipEveryXImageLineEdit.text())
        # except ValueError:
        #     self._skipXfiles = 1

        num_images = len(self._image_files)
        self._image_info = Image()
        if not self._image_info.dicom(self._image_files[0]):
            return False
        self._image_info.setNumberOfImages(num_images)

        self._ui.imageSlider.setMaximum(self._image_info.getNumberOfImages() - 1)
        # self._ui.imageSlider.setSingleStep(self._skipXfiles)
        self._ui.imageSlider.valueChanged.connect(self._zinc.displayImageandNodes)
        self._zinc.update_slider.connect(self.displayImage)

        self._zinc.setCurrentImageSet(0, num_images)

        if self._zinc.loadImages(self._image_files, self._image_info):
            return True

        print("Error occured")
        return False

    def selectImages(self):
        """
        Gets the file names for the images and then sets a flag and then checks if all the conditions have been met.
        :return:
        """
        self._image_files, filter = QtGui.QFileDialog.getOpenFileNames(self, u"Please select the DICOM image")

        if len(self._image_files):
            self._valid_images = True
        else:
            self._valid_images = False

        self.completeChangedImage()

    def createNodes(self):
        """
        Opens up an editor to add nodes to the images.
        :return:
        """
        if not self.node_editing_dialouge:
            self.node_editing_dialouge = FastMesher(MainUI, self._zinc)

        distance = self._image_info.getDistance()
        if self._zinc.createNodes(distance):
            self.node_editing_dialouge.getZincWidget().setContext(self._context)
            image_set = self._zinc.getCurrentImageSet()
            self.node_editing_dialouge.getZincWidget().setCurrentImageSet(image_set)
            self.node_editing_dialouge._ui.imageSlider.setMaximum(self._image_info.getNumberOfImages() - 1)
            # self.node_editing_dialouge._ui.imageSlider.setSingleStep(self._skipXfiles)
            self.node_editing_dialouge.getZincWidget().initializeTool()
            # self.node_editing_dialouge.getZincWidget()._interactiveTool.clearSelection()
            self.node_editing_dialouge.show()

    def loadNodes(self):
        """
        Loads in the Nodes from an .exnode file and checks they are valid.
        TODO to increase the level of detail that checks it.
        :return:
        """

        node_file, filter = QtGui.QFileDialog.getOpenFileName(self, u"Please select the nodes file")

        if node_file:
            self._valid_nodes = True
            try:
                node_file = node_file.encode('ascii')
            except UnicodeEncodeError:
                print("Unsupported file characters")
                self._valid_nodes = False
                return
            self._zinc.loadNodes(node_file)

        else:
            self._valid_nodes = False

        self._ui.NodePage.completeChanged.emit()

    def generateMesh(self):
        """
        Function called when the Generate Mesh button is pressed.
        :return:
        """
        if self._zinc.generateMesh():
            self._valid_meshes = True
            # self.next()
        else:
            self._valid_meshes = False

        self._ui.MeshPage.completeChanged.emit()

    def loadMesh(self):
        """
        Loads in the Nodes from an .exnode file and checks they are valid.
        TODO to increase the level of detail that checks it.
        :return:
        """
        mesh_files, filter = QtGui.QFileDialog.getOpenFileNames(self, u"Please select the pre-generated mesh files")
        if mesh_files:
            self._valid_meshes = True
            if self._zinc.loadMesh(mesh_files):
                # self.next()
                pass
        else:
            self._valid_meshes = False

        self._ui.MeshPage.completeChanged.emit()

        # TODO add the actual loading of the Mesh

    @QtCore.Slot(QtGui.QRadioButton)
    def meshVisibility(self, button):
        text = button.text()
        self._zinc.viewMesh(text)

    @QtCore.Slot(int)
    def displayImage(self, change):
        image_slider = self._ui.imageSlider
        value = change + image_slider.value()

        if image_slider.maximum() < value or image_slider.minimum() > value:
            return
        image_slider.setValue(value)
        self._zinc.viewImage(value)

    @QtCore.Slot(bool)
    def toggleBoundingBox(self, state):
        zinc = self._zinc

        zinc.toggleVisibility(state, 'BB_Points')
        zinc.toggleVisibility(state, 'BB_Lines')

    @QtCore.Slot(bool)
    def toggleNodes(self, state):
        self._zinc.viewNodes(state)

    @QtCore.Slot(bool)
    def toggleIsoLines(self, state):
        self._zinc.toggleIsolines(state)

    @QtCore.Slot(bool)
    def toggleImages(self, state):
        self._zinc.toggleImages(state)
        if state is False:
            self._zinc.toggleIsolines(False)
        else:
            self._zinc.toggleIsolines(self._ui.displayIsoLines.isChecked())

    @QtCore.Slot(bool)
    def toggleContainer(self, state):
        self._zinc.toggleVisibility(state, 'container')

    @QtCore.Slot(bool)
    def toggleIsoSurface(self, state):
        self._zinc.toggleVisibility(state, 'IsoSurface')

    @QtCore.Slot(bool)
    def toggleMesh(self, state):
        self._zinc.toggleVisibility(state, 'Generated_Mesh')

    @QtCore.Slot()
    def updateVolume(self, volumes):
        """
        Updates the UI with the volumes
        :volumes: A dictionary containing the volumes
        :return:
        """

        ui = self._ui

        for num, volume in volumes.iteritems():
            _label = getattr(ui, "vol_{}".format(num))
            getattr(_label, 'setText')("{0:,.0f}".format(volume))


class FastMesher(QtGui.QWidget):
    """
    This is the main class of the program. It includes the first and second stages
    """

    def __init__(self, ui_class, shared=None):
        super(FastMesher, self).__init__()

        self._ui = None
        self.setupUI(ui_class, shared)

        # Set up parameters
        self._zinc_reference = None
        self._context = None
        self._volume = None

        self._zinc_reference = self._ui.zinc_widget
        self.setUpConnections()

    def setupUI(self, ui, shared=None):
        self._ui = ui()
        self._ui.setupUi(self, shared)

    def getUI(self):
        if self._ui is None:
            raise AttributeError
        return self._ui

    def getZincWidget(self):
        if self._zinc_reference is not None:
            return self._ui.zinc_widget
        else:
            raise AttributeError

    def setUpConnections(self):
        ui = self._ui
        zinc = self._zinc_reference

        ui.viewAllButton.clicked.connect(zinc.viewAll)
        ui.saveButton.released.connect(self.saveNodesToFile)
        ui.displayIsoLines.toggled.connect(zinc.toggleIsolines)
        ui.displayContainer.toggled.connect(self.toggleContainer)
        ui.displayImages.toggled.connect(self.toggleImages)
        ui.displayNodes.toggled.connect(self.toggleNodes)

        ui.redoButton.clicked.connect(zinc.redo)
        ui.undoButton.clicked.connect(zinc.undo)

        image_slider = ui.imageSlider
        image_slider.valueChanged.connect(zinc.displayImageandNodes)
        zinc.update_slider.connect(self.displayImage)
        image_slider.setValue(0)
        image_slider.setInvertedAppearance(True)

        # zinc._interactiveTool._undo.undo_state.connect(self.toggleUndo)
        # zinc._interactiveTool._undo.redo_state.connect(self.toggleRedo)

        ui.interactGroup.buttonClicked.connect(self.updateNodeMode)

    @QtCore.Slot(int)
    def displayImage(self, change):
        image_slider = self._ui.imageSlider
        value = change + image_slider.value()

        if image_slider.maximum() < value or image_slider.minimum() > value:
            return
        image_slider.setValue(value)
        self._zinc_reference.viewImage(value)

    @QtCore.Slot(bool)
    def toggleNodes(self, state):
        self._zinc_reference.viewNodes(state)

    @QtCore.Slot(bool)
    def toggleIsoLines(self, state):
        self._zinc_reference.toggleIsolines(state)

    @QtCore.Slot(bool)
    def toggleImages(self, state):
        self._zinc_reference.toggleImages(state)
        if state is False:
            self._zinc_reference.toggleIsolines(False)
        else:
            self._zinc_reference.toggleIsolines(self._ui.displayIsoLines.isChecked())

    @QtCore.Slot(QtGui.QRadioButton)
    def updateNodeMode(self):
        """
        Makes the Zinc Widget update the node mode by figuring out which radio button has been toggled
        :return: None
        """
        ui = self._ui
        zincInteract = ui.zinc_widget._interactiveTool

        if ui.viewNodeButton.isChecked():
            zincInteract.setNodeCreateMode(False)
            zincInteract.setNodeSelection(False)
            zincInteract.setNodeEdit(False)
            zincInteract.setNodeDeleteMode(False)
            zincInteract.setSelectionModeAdditive(False)

        elif ui.createNodeButton.isChecked():
            zincInteract.setNodeCreateMode(True)
            zincInteract.setNodeSelection(True)
            zincInteract.setNodeEdit(True)
            zincInteract.setNodeDeleteMode(False)
            zincInteract.setSelectionModeAdditive(True)

        elif ui.moveNodeButton.isChecked():
            zincInteract.setNodeCreateMode(False)
            zincInteract.setNodeSelection(True)
            zincInteract.setNodeEdit(True)
            zincInteract.setNodeDeleteMode(False)
            zincInteract.setSelectionModeAdditive(False)

        elif ui.deleteNodeButton.isChecked():
            zincInteract.setNodeCreateMode(False)
            zincInteract.setNodeSelection(True)
            zincInteract.setNodeEdit(False)
            zincInteract.setNodeDeleteMode(True)
            zincInteract.setSelectionModeAdditive(False)

    def saveNodesToFile(self):

        ui = self._ui
        zinc = self._zinc_reference

        filename, extension = QtGui.QFileDialog.getSaveFileName(self, "Select a directory to save", "", "EX node format (*.exnode)")

        # Checks for if cancel was pressed.
        if not filename and not extension:
            return

        if os.path.isfile(filename):
            if os.path.isfile(filename + ".backup"):
                os.remove(filename + ".backup")
            os.rename(filename, filename + ".backup")

        if zinc.saveNodesToFile(filename):
            text = "File successfully saved as {}".format(filename)
        else:
            text = "Error saving file".format()
        QtGui.QMessageBox.about(self, "File Status", text)

    def toggleContainer(self, state):
        self._zinc_reference.toggleVisibility(state, 'container')

    @QtCore.Slot(bool)
    def toggleBoundingBox(self, state):
        zinc = self._zinc_reference

        zinc.toggleVisibility(state, 'BB_Points')
        zinc.toggleVisibility(state, 'BB_Lines')

    @QtCore.Slot(int)
    def changeImage(self, value):
        zinc = self._zinc_reference
        zinc.setCurrentImageSet(value)
        zinc.viewImages()
        zinc.viewNodes()

    @QtCore.Slot(bool)
    def toggleUndo(self, State):
        # TODO reimplement this
        self._ui.undoButton.setDisabled(not State)
        pass

    @QtCore.Slot(bool)
    def toggleRedo(self, State):
        # TODO turn back on
        self._ui.redoButton.setDisabled(not State)
        pass

    @QtCore.Slot()
    def updateVolume(self, volumes):
        """
        Updates the UI with the volumes
        :volumes: A dictionary containing the volumes
        :return:
        """

        ui = self._ui

        for num, volume in volumes.iteritems():
            _label = getattr(ui, "vol_{}".format(num))
            getattr(_label, 'setText')("{0:,.0f}".format(volume))


# main start
def main():
    """
    The entry point for the application, handle application arguments and initialise the GUI.
    """

    app = QtGui.QApplication(sys.argv)

    w = ImportWizard()
    w.show()
    sys.exit(app.exec_())
# main end

if __name__ == '__main__':
    main()
