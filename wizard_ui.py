# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'wizard.ui'
#
# Created: Wed Feb 17 17:37:02 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_wizard(object):
    def setupUi(self, wizard):
        wizard.setObjectName("wizard")
        wizard.setWindowModality(QtCore.Qt.NonModal)
        wizard.resize(829, 565)
        wizard.setWizardStyle(QtGui.QWizard.ModernStyle)
        wizard.setOptions(QtGui.QWizard.HelpButtonOnRight)
        self.ImagePage = QtGui.QWizardPage()
        self.ImagePage.setObjectName("ImagePage")
        self.horizontalLayout = QtGui.QHBoxLayout(self.ImagePage)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.textBrowser_4 = QtGui.QTextBrowser(self.ImagePage)
        self.textBrowser_4.setObjectName("textBrowser_4")
        self.horizontalLayout.addWidget(self.textBrowser_4)
        self.verticalLayout_3 = QtGui.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem)
        self.loadImages = QtGui.QPushButton(self.ImagePage)
        self.loadImages.setMinimumSize(QtCore.QSize(200, 150))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.loadImages.setFont(font)
        self.loadImages.setObjectName("loadImages")
        self.verticalLayout_3.addWidget(self.loadImages)
        spacerItem1 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem1)
        self.horizontalLayout.addLayout(self.verticalLayout_3)
        self.textBrowser = QtGui.QTextBrowser(self.ImagePage)
        self.textBrowser.setObjectName("textBrowser")
        self.horizontalLayout.addWidget(self.textBrowser)
        wizard.addPage(self.ImagePage)
        self.NodePage = QtGui.QWizardPage()
        self.NodePage.setObjectName("NodePage")
        self.horizontalLayout_2 = QtGui.QHBoxLayout(self.NodePage)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.textBrowser_5 = QtGui.QTextBrowser(self.NodePage)
        self.textBrowser_5.setObjectName("textBrowser_5")
        self.horizontalLayout_2.addWidget(self.textBrowser_5)
        self.verticalLayout_4 = QtGui.QVBoxLayout()
        self.verticalLayout_4.setContentsMargins(50, -1, 50, -1)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.createNodes = QtGui.QPushButton(self.NodePage)
        self.createNodes.setMinimumSize(QtCore.QSize(160, 150))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.createNodes.setFont(font)
        self.createNodes.setObjectName("createNodes")
        self.verticalLayout_4.addWidget(self.createNodes)
        self.loadNodes = QtGui.QPushButton(self.NodePage)
        self.loadNodes.setMinimumSize(QtCore.QSize(160, 150))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.loadNodes.setFont(font)
        self.loadNodes.setObjectName("loadNodes")
        self.verticalLayout_4.addWidget(self.loadNodes)
        self.horizontalLayout_2.addLayout(self.verticalLayout_4)
        self.textBrowser_2 = QtGui.QTextBrowser(self.NodePage)
        self.textBrowser_2.setObjectName("textBrowser_2")
        self.horizontalLayout_2.addWidget(self.textBrowser_2)
        wizard.addPage(self.NodePage)
        self.MeshPage = QtGui.QWizardPage()
        self.MeshPage.setObjectName("MeshPage")
        self.horizontalLayout_3 = QtGui.QHBoxLayout(self.MeshPage)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.textBrowser_6 = QtGui.QTextBrowser(self.MeshPage)
        self.textBrowser_6.setObjectName("textBrowser_6")
        self.horizontalLayout_3.addWidget(self.textBrowser_6)
        self.verticalLayout_5 = QtGui.QVBoxLayout()
        self.verticalLayout_5.setContentsMargins(50, -1, 50, -1)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.generateMesh = QtGui.QPushButton(self.MeshPage)
        self.generateMesh.setEnabled(False)
        self.generateMesh.setMinimumSize(QtCore.QSize(160, 150))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.generateMesh.setFont(font)
        self.generateMesh.setObjectName("generateMesh")
        self.verticalLayout_5.addWidget(self.generateMesh)
        self.loadMesh = QtGui.QPushButton(self.MeshPage)
        self.loadMesh.setMinimumSize(QtCore.QSize(160, 150))
        font = QtGui.QFont()
        font.setPointSize(16)
        self.loadMesh.setFont(font)
        self.loadMesh.setObjectName("loadMesh")
        self.verticalLayout_5.addWidget(self.loadMesh)
        self.horizontalLayout_3.addLayout(self.verticalLayout_5)
        self.textBrowser_7 = QtGui.QTextBrowser(self.MeshPage)
        self.textBrowser_7.setObjectName("textBrowser_7")
        self.horizontalLayout_3.addWidget(self.textBrowser_7)
        wizard.addPage(self.MeshPage)
        self.PreviewPage = QtGui.QWizardPage()
        self.PreviewPage.setObjectName("PreviewPage")
        self.horizontalLayout_4 = QtGui.QHBoxLayout(self.PreviewPage)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.frame = QtGui.QFrame(self.PreviewPage)
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.gridLayout = QtGui.QGridLayout(self.frame)
        self.gridLayout.setObjectName("gridLayout")
        self.vol_6 = QtGui.QLabel(self.frame)
        self.vol_6.setObjectName("vol_6")
        self.gridLayout.addWidget(self.vol_6, 5, 2, 1, 1)
        self.vol_4 = QtGui.QLabel(self.frame)
        self.vol_4.setObjectName("vol_4")
        self.gridLayout.addWidget(self.vol_4, 4, 2, 1, 1)
        self.vol_1 = QtGui.QLabel(self.frame)
        self.vol_1.setObjectName("vol_1")
        self.gridLayout.addWidget(self.vol_1, 3, 0, 1, 1)
        self.line_3 = QtGui.QFrame(self.frame)
        self.line_3.setFrameShape(QtGui.QFrame.VLine)
        self.line_3.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.gridLayout.addWidget(self.line_3, 3, 1, 4, 1)
        self.vol_8 = QtGui.QLabel(self.frame)
        self.vol_8.setObjectName("vol_8")
        self.gridLayout.addWidget(self.vol_8, 6, 2, 1, 1)
        self.vol_5 = QtGui.QLabel(self.frame)
        self.vol_5.setObjectName("vol_5")
        self.gridLayout.addWidget(self.vol_5, 5, 0, 1, 1)
        self.vol_7 = QtGui.QLabel(self.frame)
        self.vol_7.setObjectName("vol_7")
        self.gridLayout.addWidget(self.vol_7, 6, 0, 1, 1)
        self.vol_2 = QtGui.QLabel(self.frame)
        self.vol_2.setObjectName("vol_2")
        self.gridLayout.addWidget(self.vol_2, 3, 2, 1, 1)
        self.vol_3 = QtGui.QLabel(self.frame)
        self.vol_3.setObjectName("vol_3")
        self.gridLayout.addWidget(self.vol_3, 4, 0, 1, 1)
        self.line_4 = QtGui.QFrame(self.frame)
        self.line_4.setFrameShape(QtGui.QFrame.HLine)
        self.line_4.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_4.setObjectName("line_4")
        self.gridLayout.addWidget(self.line_4, 2, 0, 1, 3)
        self.label_4 = QtGui.QLabel(self.frame)
        self.label_4.setObjectName("label_4")
        self.gridLayout.addWidget(self.label_4, 0, 0, 1, 2)
        self.vol_Total = QtGui.QLabel(self.frame)
        self.vol_Total.setObjectName("vol_Total")
        self.gridLayout.addWidget(self.vol_Total, 0, 2, 1, 1)
        self.verticalLayout_2.addWidget(self.frame)
        self.saveButton = QtGui.QPushButton(self.PreviewPage)
        self.saveButton.setObjectName("saveButton")
        self.verticalLayout_2.addWidget(self.saveButton)
        self.line = QtGui.QFrame(self.PreviewPage)
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout_2.addWidget(self.line)
        self.displayMesh = QtGui.QCheckBox(self.PreviewPage)
        self.displayMesh.setChecked(True)
        self.displayMesh.setObjectName("displayMesh")
        self.verticalLayout_2.addWidget(self.displayMesh)
        self.displayBoundingBox = QtGui.QCheckBox(self.PreviewPage)
        self.displayBoundingBox.setChecked(True)
        self.displayBoundingBox.setObjectName("displayBoundingBox")
        self.verticalLayout_2.addWidget(self.displayBoundingBox)
        self.displayIsoSurface = QtGui.QCheckBox(self.PreviewPage)
        self.displayIsoSurface.setChecked(False)
        self.displayIsoSurface.setObjectName("displayIsoSurface")
        self.verticalLayout_2.addWidget(self.displayIsoSurface)
        self.displayNodes = QtGui.QCheckBox(self.PreviewPage)
        self.displayNodes.setChecked(False)
        self.displayNodes.setObjectName("displayNodes")
        self.verticalLayout_2.addWidget(self.displayNodes)
        self.displayImages = QtGui.QCheckBox(self.PreviewPage)
        self.displayImages.setChecked(False)
        self.displayImages.setObjectName("displayImages")
        self.verticalLayout_2.addWidget(self.displayImages)
        self.displayIsoLines = QtGui.QCheckBox(self.PreviewPage)
        self.displayIsoLines.setChecked(False)
        self.displayIsoLines.setObjectName("displayIsoLines")
        self.verticalLayout_2.addWidget(self.displayIsoLines)
        self.displayContainer = QtGui.QCheckBox(self.PreviewPage)
        self.displayContainer.setChecked(False)
        self.displayContainer.setObjectName("displayContainer")
        self.verticalLayout_2.addWidget(self.displayContainer)
        self.textBrowser_3 = QtGui.QTextBrowser(self.PreviewPage)
        self.textBrowser_3.setObjectName("textBrowser_3")
        self.verticalLayout_2.addWidget(self.textBrowser_3)
        self.horizontalLayout_4.addLayout(self.verticalLayout_2)
        self.verticalLayout_6 = QtGui.QVBoxLayout()
        self.verticalLayout_6.setObjectName("verticalLayout_6")
        self.zincWidget = ZincWidget(self.PreviewPage)
        self.zincWidget.setMinimumSize(QtCore.QSize(600, 0))
        self.zincWidget.setObjectName("zincWidget")
        self.verticalLayout = QtGui.QVBoxLayout(self.zincWidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.verticalLayout_6.addWidget(self.zincWidget)
        self.imageSlider = QtGui.QSlider(self.PreviewPage)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.imageSlider.sizePolicy().hasHeightForWidth())
        self.imageSlider.setSizePolicy(sizePolicy)
        self.imageSlider.setMinimumSize(QtCore.QSize(0, 0))
        self.imageSlider.setMaximum(10)
        self.imageSlider.setPageStep(5)
        self.imageSlider.setOrientation(QtCore.Qt.Horizontal)
        self.imageSlider.setInvertedAppearance(False)
        self.imageSlider.setTickPosition(QtGui.QSlider.TicksBothSides)
        self.imageSlider.setTickInterval(1)
        self.imageSlider.setObjectName("imageSlider")
        self.verticalLayout_6.addWidget(self.imageSlider)
        self.horizontalLayout_4.addLayout(self.verticalLayout_6)
        wizard.addPage(self.PreviewPage)

        self.retranslateUi(wizard)
        QtCore.QMetaObject.connectSlotsByName(wizard)

    def retranslateUi(self, wizard):
        wizard.setWindowTitle(QtGui.QApplication.translate("wizard", "Liver Mesher/Viewer", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser_4.setHtml(QtGui.QApplication.translate("wizard", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:600; color:#0000ff;\">1. Images</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:600;\">2. Nodes</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:600;\">3. Mesh</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:600;\">4. Finish</span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.loadImages.setText(QtGui.QApplication.translate("wizard", "Load Images", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser.setHtml(QtGui.QApplication.translate("wizard", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt;\">Step - 1: Images </span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">Images need to be loaded in sequence to work. Currently only DICOM images are supported as they have the required tags for Slice thickness, size and pixel spacing. </span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser_5.setHtml(QtGui.QApplication.translate("wizard", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:600; color:#000000;\">1. Images</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:600; color:#0000ff;\">2. Nodes</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:600;\">3. Mesh</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:600;\">4. Finish</span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.createNodes.setText(QtGui.QApplication.translate("wizard", "Create Nodes", None, QtGui.QApplication.UnicodeUTF8))
        self.loadNodes.setText(QtGui.QApplication.translate("wizard", "Load Nodes", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser_2.setHtml(QtGui.QApplication.translate("wizard", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt;\">Step 2 - Nodes</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:16pt;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">The nodes can either be loaded in from a file or created using the built in node-creation tool. Saving Nodes will automatically put the nodes in to the Node_Cloud group.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">Loaded nodes need to be in the node group: Node_Cloud. In future versions you will be able to either specify the node group or have it automatically generated. Nodes and Elements must begin at 100 or more to avoid conflicts. </span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser_6.setHtml(QtGui.QApplication.translate("wizard", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:600; color:#000000;\">1. Images</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:600; color:#000000;\">2. Nodes</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:600; color:#0000ff;\">3. Mesh</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt; font-weight:600;\">4. Finish</span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.generateMesh.setText(QtGui.QApplication.translate("wizard", "Generate Mesh", None, QtGui.QApplication.UnicodeUTF8))
        self.loadMesh.setText(QtGui.QApplication.translate("wizard", "Load Mesh", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser_7.setHtml(QtGui.QApplication.translate("wizard", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt;\">Step 3 - Mesh </span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:16pt;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt;\">Generate Mesh:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">To be implemented.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:16pt;\">Load Mesh:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">Loads in a mesh that is created in CMGUI. It needs to be a 3D mesh in order for the Volume Computation to work.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt; font-weight:600;\">Coordinate Field Name</span><span style=\" font-size:12pt;\">: mesh_coordinates</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt; font-weight:600;\">Group Name</span><span style=\" font-size:12pt;\">: mesh</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;\"><br /></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;\"><br /></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))
        self.vol_6.setText(QtGui.QApplication.translate("wizard", "TextLabel", None, QtGui.QApplication.UnicodeUTF8))
        self.vol_4.setText(QtGui.QApplication.translate("wizard", "TextLabel", None, QtGui.QApplication.UnicodeUTF8))
        self.vol_1.setText(QtGui.QApplication.translate("wizard", "TextLabel", None, QtGui.QApplication.UnicodeUTF8))
        self.vol_8.setText(QtGui.QApplication.translate("wizard", "TextLabel", None, QtGui.QApplication.UnicodeUTF8))
        self.vol_5.setText(QtGui.QApplication.translate("wizard", "TextLabel", None, QtGui.QApplication.UnicodeUTF8))
        self.vol_7.setText(QtGui.QApplication.translate("wizard", "TextLabel", None, QtGui.QApplication.UnicodeUTF8))
        self.vol_2.setText(QtGui.QApplication.translate("wizard", "TextLabel", None, QtGui.QApplication.UnicodeUTF8))
        self.vol_3.setText(QtGui.QApplication.translate("wizard", "TextLabel", None, QtGui.QApplication.UnicodeUTF8))
        self.label_4.setText(QtGui.QApplication.translate("wizard", "Total Volume:", None, QtGui.QApplication.UnicodeUTF8))
        self.vol_Total.setText(QtGui.QApplication.translate("wizard", "TextLabel", None, QtGui.QApplication.UnicodeUTF8))
        self.saveButton.setText(QtGui.QApplication.translate("wizard", "Save Nodes, Mesh & Bounding Box", None, QtGui.QApplication.UnicodeUTF8))
        self.saveButton.setShortcut(QtGui.QApplication.translate("wizard", "Ctrl+S", None, QtGui.QApplication.UnicodeUTF8))
        self.displayMesh.setText(QtGui.QApplication.translate("wizard", "Display Mesh", None, QtGui.QApplication.UnicodeUTF8))
        self.displayBoundingBox.setText(QtGui.QApplication.translate("wizard", "Display Bounding Box", None, QtGui.QApplication.UnicodeUTF8))
        self.displayIsoSurface.setText(QtGui.QApplication.translate("wizard", "Display Iso-Surface", None, QtGui.QApplication.UnicodeUTF8))
        self.displayNodes.setText(QtGui.QApplication.translate("wizard", "Display Nodes", None, QtGui.QApplication.UnicodeUTF8))
        self.displayImages.setText(QtGui.QApplication.translate("wizard", "Display Images", None, QtGui.QApplication.UnicodeUTF8))
        self.displayIsoLines.setText(QtGui.QApplication.translate("wizard", "Display Iso-lines", None, QtGui.QApplication.UnicodeUTF8))
        self.displayContainer.setText(QtGui.QApplication.translate("wizard", "Display Container", None, QtGui.QApplication.UnicodeUTF8))
        self.textBrowser_3.setHtml(QtGui.QApplication.translate("wizard", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">Preview of the finished product.</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">Move the nodes to align bounding box with the vasculature.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:12pt;\">Press finish to save the result. Remember to enter a filename.</span></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))

from zinc_widget import ZincWidget
