import struct
import imghdr
import dicom
import os


class Image(object):
    """
    This class includes all the attributes of the loaded images to be easily expanded in the future.
    """
    def __init__(self, size=None, distance=None, image_number=None):
        self._size = size
        # self._resolution = resolution
        self._distance = distance
        self._number_of_images = image_number

    def getSize(self):
        if self._size is not None:
            return self._size
        else:
            raise AttributeError

    def setSize(self, size):
        self._size = size

    # def getResolution(self):
    #     if self._resolution is not None:
    #         return self._resolution
    #     else:
    #         raise AttributeError
    #
    # def setResolution(self, res):
    #     self._resolution = res

    def getDistance(self):
        if self._distance is not None:
            return self._distance
        else:
            raise AttributeError

    def setDistance(self, distance):
        self._distance = distance

    def getNumberOfImages(self):
        if self._number_of_images is not None:
            return self._number_of_images
        else:
            raise AttributeError

    def setNumberOfImages(self, num):
        self._number_of_images = num

    @staticmethod
    def get_image_size(fname):
        """Determine the image type of fhandle and return its size.
        from draco"""
        with open(fname, 'rb') as fhandle:
            head = fhandle.read(24)
            if len(head) != 24:
                return
            if imghdr.what(fname) == 'png':
                check = struct.unpack('>i', head[4:8])[0]
                if check != 0x0d0a1a0a:
                    return
                width, height = struct.unpack('>ii', head[16:24])
            elif imghdr.what(fname) == 'gif':
                width, height = struct.unpack('<HH', head[6:10])
            elif imghdr.what(fname) == 'jpeg':
                try:
                    fhandle.seek(0) # Read 0xff next
                    size = 2
                    ftype = 0
                    while not 0xc0 <= ftype <= 0xcf:
                        fhandle.seek(size, 1)
                        byte = fhandle.read(1)
                        while ord(byte) == 0xff:
                            byte = fhandle.read(1)
                        ftype = ord(byte)
                        size = struct.unpack('>H', fhandle.read(2))[0] - 2
                    # We are at a SOFn block
                    fhandle.seek(1, 1)  # Skip `precision' byte.
                    height, width = struct.unpack('>HH', fhandle.read(4))
                except Exception: # IGNORE:W0703
                    return
            elif os.path.splitext(fname)[1] == '.dcm' or os.path.splitext(fname)[1] == '.dicom':
                Dicom = dicom.read_file(fname)
                size = Dicom.PixelSpacing
                width = size[0] * Dicom.Rows
                height = size[1] * Dicom.Columns
            else:
                return
            return width, height

    def dicom(self, filename):
        try:
            Dicom = dicom.read_file(filename)
        except dicom.handler.InvalidDicomError:
            return False
        size = Dicom.PixelSpacing
        self._size = 2 * [0]
        self._size[0] = size[0] * Dicom.Columns
        self._size[1] = size[1] * Dicom.Rows
        self._distance = Dicom.SliceThickness
        return True

